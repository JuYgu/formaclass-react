import React from 'react';
import {ErrorMessage, Field} from 'formik';

import './input.css';

import TextField from '@material-ui/core/TextField';

// Voici notre input réutilisable. Il a comme props un label, un name et type. Celui est optionel. Le type par défaut sera "text"
const FormikField = ({label, name, type = "text", required=true, error, placeholder, defautValue=""}) => {
    return (
        <div className="FormikField">
            <Field
               // autoComplete="off"
                as={TextField}
                type={type}
                name={name}
                label={label}
                fullWidth
                required={required}
                helperText={<ErrorMessage name={name} className="errorMessage"/>}
                error={error}
                placeholder={placeholder}
                defautValue={defautValue}

            />
        </div>
    );
};

export default FormikField;
