import React from 'react';

import './select.css';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Field, ErrorMessage } from 'formik';


const GenericSelectField = ({children, label, errorString, value, name, onChange, onBlur, required = true}) =>{
    return (
        <FormControl fullWidth>
        <InputLabel required={required}>{label}</InputLabel>
        <Select name={name} onChange={onChange} onBlur={onBlur} value={value}>
            {children}
        </Select>
        <FormHelperText>{errorString}</FormHelperText>
    </FormControl>
    )
}


const FormikSelect = ({name ,label, categories, defautValue=""}) =>{
    return (
        <div className="formikSelect">
            <Field name={name} as={GenericSelectField} label={label} defautValue={defautValue} errorString={<ErrorMessage name={name} />}>
                {categories.map(categorie => (
                        <MenuItem key={categorie.idCategorie} value={categorie.idCategorie}>
                            {categorie.type}
                        </MenuItem>
                    ))}
            </Field>
        </div>
    )
}

export default FormikSelect;
