import React, { Component } from 'react'
import { TextField } from '@material-ui/core';

export default class SearchBar extends Component {
    render() {
        return (
            <>
                 <form className="form">
                        <TextField
                            label="rechercher"
                            onChange = {this.props.handleSearch}
                        />
                    </form>
            </>
        )
    }
}
