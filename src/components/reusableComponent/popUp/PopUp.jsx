import { Dialog, DialogContent, DialogTitle, makeStyles} from '@material-ui/core';
import React from 'react'
import CloseIcon from '@material-ui/icons/Close'
import ActionButton from './../button/ActionButton';

const useStyles = makeStyles(theme =>({
    dialogWrapper : {
        padding : theme.spacing(2),
        position: "absolute",
        top: theme.spacing(5),
        backgroundColor: "#ffffffe8"
    }

}))

export default function PopUp(props) {

    const { title, children, openPopup, setOpenPopup } = props;
    const classes = useStyles();

    return (
        <Dialog open={openPopup} classes={{ paper :classes.dialogWrapper}} >
            <DialogTitle>
                <div>
                    {title}
                    <ActionButton color="error" onClick = { () => {setOpenPopup(false)}}  ><CloseIcon color="error"/></ActionButton>
                </div>

            </DialogTitle>
            <DialogContent dividers>
                {children}
            </DialogContent>
        </Dialog>
    )
}
