
import React, {useState} from 'react'
import {ErrorMessage, Field} from 'formik';
import RadioGroup from '@material-ui/core/RadioGroup'
import  FormLabel  from '@material-ui/core/FormLabel';
import  FormControl  from '@material-ui/core/FormControl';
import  FormControlLabel  from '@material-ui/core/FormControlLabel';
import  Radio  from '@material-ui/core/Radio';
import  FormHelperText  from '@material-ui/core/FormHelperText';

const GenericOptionSalle = ({children, label, errorString,  name, onChange, onBlur, required = true}) => {
    const [value, setValue] = useState("")
   
    const handleClick = (e) => {
        if(e.target.value === value) {
            setValue("")
        }else{
            setValue(e.target.value)
        }
    }
    return (
        <>
            <FormControl>
            <FormLabel component="legend">{label}</FormLabel>
            <RadioGroup  name={name} onChange={onChange} onBlur={onBlur} value={value}>
                {children }
            </RadioGroup>
            <FormHelperText>{errorString}</FormHelperText>
            </FormControl>
        </>
    )
}

export default function FormikOption (props) {
    const {name ,label, salles} = props

    const [value, setValue] = useState("")
   
    const handleClick = (e) => {
        if(e.target.value === value) {
            setValue("")
        }else{
            setValue(e.target.value)
        }
    }
    return (
        <div className="formikSelect">
            <Field name={name} as={GenericOptionSalle} label={label}  errorString={<ErrorMessage name={name} />}>
               {
                    salles.map(salle =>(
                        <FormControlLabel 
                            key={salle.idSalle}
                            control={<Radio onClick={handleClick}/>}
                            value={salle.id} 
                            label={salle.nom}
                            color="primary"
                        />
                        
                        ))
               }
            </Field>
        </div>
    )
}