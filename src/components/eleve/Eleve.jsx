import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import EleveService from '../../services/eleve/EleveService';
import './../../css/service-eleve.css';


export default class Eleve extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idEleve: this.props.match.params.id,
            nom: '',
            prenom: "",
            voie: "",
            ville: "",
            codePostal: "",
            telephone: "",
            specialite : "",
            actif : true,
            type: "",
            email: "",
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        console.log("eleve data " + this.state.idEleve)
        EleveService.getEleve(this.state.idEleve).then((response) => {

            console.log("eleve data" + response)
            this.setState({ nom: response.data.nom,
                            prenom : response.data.prenom,
                            voie : response.data.voie,
                            ville : response.data.ville,
                            codePostal : response.data.codePostal,
                            telephone : response.data.telephone,
                            actif : response.data.actif,
                            email : response.data.email,
                           
            });           
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    editEleve(id){
        this.props.history.push(`/editer-eleve/${id}`);
    }

    supprimerEleve(id){

        EleveService.deleteEleve(id);
        this.listeEleve();
        this.window.location.reload();
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    listeEleve = () => {
        this.props.history.push("/liste-eleves");
    }

    render() {

        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card>
                    
                            <tr className ="login" key={this.state.idEleve} >
                                <td> Nom : {this.state.nom} {this.state.prenom}</td>
                                <td> Actif : {this.state.actif === true ? 'oui' : 'non'}</td>
                            {/* <td> Form en cours : recherche sur la table session join avec la table eleve afin de recuperer les sessions par l'id eleve join avec la table statut pour récuperer celle au statut "en cours"</td> */}
                                <td>  
                               
                                <Button color="primary" variant="contained" onClick={() => this.editEleve(this.state.idEleve)}>modifier</Button>
                                <Button color="primary" variant="contained" onClick={() => this.supprimerEleve(this.state.idEleve)}>supprimer</Button>
                                </td>
                            </tr>
                    </Card>
                     <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
            </Container>
        )
    }

}


