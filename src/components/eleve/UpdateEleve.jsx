import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Formik} from 'formik';
import FormikField from '../reusableComponent/formikField/Input';
import EleveService from '../../services/eleve/EleveService';
import './../../css/service-eleve.css';


export default class UpdateEleve extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idEleve: this.props.match.params.id,
            nom: '',
            prenom: "",
            voie: "",
            ville: "",
            codePostal: "",
            telephone: "",
            email: "",
            note: "",
            actif : true,
        }
    }

    componentDidMount() {

        console.log("eleve data " + this.state.idEleve)
        EleveService.getEleve(this.state.idEleve).then((response) => {

            console.log("eleve data" + response)
            this.setState({ nom: response.data.nom,
                            prenom : response.data.prenom,
                            voie : response.data.voie,
                            ville : response.data.ville,
                            codePostal : response.data.codePostal,
                            telephone : response.data.telephone,
                            email : response.data.email,
                            note : response.data.note
                            
            });           
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    ajoutEleveSchema = Yup.object().shape({
        nom: Yup.string().min(1, "Minimum 1 caractère").max(50, "Maximum 20 caracteres").required("Veuillez indiquer votre ville"),
        prenom: Yup.string().min(1, "indiquez une capacité de eleve minimale").max(50).required("Veuillez indiquer votre prénom"),
        voie: Yup.string().min(6, "Minimum 6 caractère").max(60, "Maximum 60 caracteres").required("Veuillez renseigner votre adresse"),
        ville: Yup.string().min(1, "Minimum 1 caractère").max(40, "Maximum 40 caracteres").required("Veuillez indiquer votre ville"),
        codePostal: Yup.string().matches(/^\d+$/).min(5, "veuillez indiquer les 5 chiffres de votre code postal").max(5).required("Veuillez indiquer votre code postal"),
        telephone: Yup.string().matches(/^\d+$/).min(10, "veuillez indiquer les 10 chiffres de votre numéro de téléphone").max(10).required("Veuillez indiquer votre numéro de téléphone"),
    })

    authUserSchema = Yup.object().shape({
        authUser: Yup.object().shape({
            email: Yup.string().email().required("Veuillez indiquer votre email")
            
        })
    })

    initialValues = {

        nom: "",
        prenom: "",
        voie: "",
        ville: "",
        codePostal: "",
        telephone: "",
        email: "",
        note: ""
    }

    handleSubmit = (values) => {

        EleveService.editEleve(this.state.idEleve, values.nom, values.prenom, values.voie, values.ville, values.codePostal, values.telephone, values.email, values.note);
        this.props.history.push("/liste-eleves");
        this.window.location.reload();
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    render() {

    
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card>
                <Formik initialValues={this.initialValues}
                            onSubmit={this.handleSubmit}
                            validationSchema={this.ajoutEleveSchema}
                            enableReinitialize={true}
                        >
                            <Form>
                            <tr className ="login" key={this.state.idEleve} >
                            <FormikField label="Nom" placeholder={this.state.nom} name="nom" />
                            <FormikField label="Prenom" placeholder={this.state.prenom} name="prenom" />
                            <FormikField label="Voie" placeholder={this.state.voie} name="voie" />
                            <FormikField label="Code postal" placeholder={this.state.codePostal} name="codePostal" />
                            <FormikField label="Ville" placeholder={this.state.ville} name="ville" />
                            <FormikField label="Telephone" placeholder={this.state.telephone} name="telephone" />
                            <FormikField label="Email" placeholder={this.state.email} name="email" />
                            <FormikField label="Note" placeholder={this.state.note} name="note" />
                            <div className="button">
                                <Button variant="contained" color="primary" type="submit" >
                                    Valider
                                </Button>
                            </div>
                            </tr>
                            </Form>
                           
                        </Formik>
                    </Card>
                   <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
            </Container>
        )
    }

}


