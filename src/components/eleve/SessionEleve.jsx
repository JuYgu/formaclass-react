import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import EleveService from '../../services/eleve/EleveService';
import './../../css/service-eleve.css';


export default class SessionEleve extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idEleve: this.props.match.params.id,
            nomination :"",
            dateDebut: "",
            dateFin: "",
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        console.log("eleve data " + this.state.idEleve)
        EleveService.getSessionEleve(this.state.idEleve).then((response) => {

            console.log("eleve data" + response)
            this.setState({ nomination: response.data.nomination,
                            dateDebut : response.data.dateDebut,
                            dateFin : response.data.dateFin,
            });           
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    listeEleve = () => {
        this.props.history.push("/liste-eleves");
    }

    render() {

    
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card>
                    
                            <tr className ="login" key={this.state.idEleve} >
                                <td> Nomination : {this.state.nomination}</td>
                                <td> Date début : {this.state.dateDebut}</td>
                                <td> Date fin : {this.state.dateFin}</td>
                            </tr>
                    </Card>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
            </Container>
        )
    }

}


