import React, { Component, useState } from 'react'
import { Box, Button, Card, CardContent, Step, StepLabel, Stepper, Container, Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Formik } from 'formik';
import FormikField from '../reusableComponent/formikField/Input';
import EleveService from '../../services/eleve/EleveService';
import './../../css/login.css';
import CategoryService from '../../services/category/Category-service';


export default class NouvelEleve extends Component {

    constructor(props) {
        super(props);

        this.state = {
            idEleve: this.props.match.params.id,
            
        }
    }

    ajoutEleveSchema = Yup.object().shape({
        nom: Yup.string().min(1, "Minimum 1 caractère").max(50, "Maximum 20 caracteres").required("Veuillez indiquer votre ville"),
        prenom: Yup.string().min(1, "indiquez une capacité de eleve minimale").max(50).required("Veuillez indiquer votre prénom"),
        voie: Yup.string().min(6, "Minimum 6 caractère").max(60, "Maximum 60 caracteres").required("Veuillez renseigner votre adresse"),
        ville: Yup.string().min(1, "Minimum 1 caractère").max(40, "Maximum 40 caracteres").required("Veuillez indiquer votre ville"),
        codePostal: Yup.string().matches(/^\d+$/).min(5, "veuillez indiquer les 5 chiffres de votre code postal").max(5).required("Veuillez indiquer votre code postal"),
        telephone: Yup.string().matches(/^\d+$/).min(10, "veuillez indiquer les 10 chiffres de votre numéro de téléphone").max(10).required("Veuillez indiquer votre numéro de téléphone"),
        email: Yup.string().email().required("Veuillez indiquer votre email"),
        note: Yup.string().min(1,"note minimale de 1").max(2,"note maximale de 10").required("Veuillez indiquer la note obtenue par l'élève")
    })


    initialValues = {

        nom: "",
        prenom: "",
        voie: "",
        ville: "",
        codePostal: "",
        telephone: "",
        email: "",
        note: ""

    }

    componentDidMount() {

        CategoryService.getAllCategory().then((response) => {

            this.setState({ listeCategories: response.data })
        });
    }

    handleSubmit = (values) => {

        EleveService.addEleve(values.nom, values.prenom, values.voie, values.ville, values.codePostal, values.telephone, values.email, values.note);
        this.props.history.push("/liste-eleves");
        this.window.location.reload();
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    listeEleve = () => {
        this.props.history.push("/liste-eleves");
    }

    render() {
        return (

            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
                <Card className="addEleve">
                    <Typography variant="h4">Ajouter un eleve</Typography>
                    <CardContent className="container">
                        <Formik initialValues={this.initialValues}
                            onSubmit={this.handleSubmit}
                            validationSchema={this.ajoutSalleSchema}
                        >
                            <Form validationSchema={this.ajoutEleveSchema}>
                                <Box paddingBottom={1}>
                                    <FormikField label="Nom" name="nom" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Prenom" name="prenom" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="voie" name="voie" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Ville" name="ville" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Code postal" name="codePostal" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Telephone" name="telephone" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Email" name="email" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Note" name="note" />
                                </Box>
                                <div className="button">
                                    <Button variant="contained" color="primary" type="submit" >
                                        Valider
                                    </Button>
                                </div>
                            </Form>
                        </Formik>
                    </CardContent>
                    <Button color="primary" variant="contained" onClick={() => { this.accueil() }}>Accueil</Button>
                </Card>
            </Container>
        );
    }
}



