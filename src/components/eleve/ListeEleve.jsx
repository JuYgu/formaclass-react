import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import EleveService from '../../services/eleve/EleveService';
import './../../css/login.css';

export default class ListeEleve extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeEleve: [],
            listeEleveUpdate: [],
            update: false,
            requeteOk: false,
            isLoaded: false,
        }

        this.afficherEleve = this.afficherEleve.bind(this);
        this.supprimerEleve = this.supprimerEleve.bind(this);
    }

    componentDidMount() {

        EleveService.listeEleves().then((response) => {
            this.setState({ listeEleve: response.data,
                             requeteOk: true, isLoaded: true  });
            console.log(this.state.listeEleve)
            
            
        
        }, error => {
        this.setState({ requeteOk: false, isLoaded: false })
    });
    }

    componentDidUpdate(prevState) {

        if(prevState === this.state.listeEleve)

        EleveService.listeEleves().then((response) => {

            this.setState({ listeEleveUpdate: response.data });
            
            if(this.state.listeEleveUpdate !== this.state.listeEleve)

             return this.setState({ listeEleve: this.state.listeEleveUpdate });
            
        });
    }

    afficherEleve(id){

        this.props.history.push(`/eleve/${id}`);
    }

    supprimerEleve(id){

        EleveService.deleteEleve(id).then(() =>
            
        this.componentDidUpdate(this.state.listeEleve));
        
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    render() {
  
   return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="login">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Actif</th>
                                <th>Categorie</th>
                                <th>Spécialités</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listeEleve.map(eleve =>
                                     
                                    <tr key={eleve.idEleve} >
                                        <td>{eleve.nom}</td>
                                        <td>{eleve.actif === true ? 'oui' : 'non'}</td>
                                        <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherEleve(eleve.idEleve)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerEleve(eleve.idEleve)}>Supprimer</Button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                 </Card>  
            </Container>
        )
    }
}
