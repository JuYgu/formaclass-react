import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import EleveService from '../../services/eleve/EleveService';
import './../../css/service-eleve.css';

export default class FormationsEleve extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeEleve: [],
            idEleve: '',
            idEnvoyee: '',
            update: false,
            requeteOk: false,
            isLoaded: false,
        }

        this.afficherEleve = this.afficherEleve.bind(this);
        this.supprimerEleve = this.supprimerEleve.bind(this);
    }

    componentDidMount() {

        EleveService.listeEleves().then((response) => {

            this.setState({ listeEleve: response.data });
        });
    }

    componentDidUpdate(){

      console.log("coucou")
        
    }

    afficherEleve(id){

        this.props.history.push(`/eleve/${id}`);
    }

    supprimerEleve(id){

        EleveService.deleteEleve(id);
        
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    render() {
        return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="tableauProf">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Categorie</th>
                                <th>Actif</th>
                                <th>Nb de form réalisées</th>
                                <th>Spécialités</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listeEleve.map(eleve =>
                                    <tr key={eleve.idEleve} >
                                        <td>{eleve.nom}</td>
                                        <td>{eleve.actif}</td>
                                        <td>nb de formations</td>{/* <td>if(`${eleve.disponible}`? "oui" : "non");</td> */}
                                        <td>{eleve.specialite}</td>{/* <td>if(`${eleve.reservable}`? "oui" : "non");</td> */}
                                        <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherEleve(eleve.idEleve)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerEleve(eleve.idEleve)}>Supprimer</Button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                    </Card>
            </Container>
        )
    }
}
