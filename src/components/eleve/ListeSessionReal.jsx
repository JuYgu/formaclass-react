import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import EleveService from '../../services/eleve/EleveService';
import './../../css/service-eleve.css';


export default class ListeSessionReal extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idEleve: this.props.match.params.id,
            listSession : [],
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        EleveService.getListSessionEleve(this.state.idEleve).then((response) => {
            console.log(response.data)
           this.setState({ listSession : response.data});  
                     
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }

    listeEleve = () => {
        this.props.history.push("/liste-eleves");
    }

    render() {

        return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="login">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nomination</th>
                                <th>Date début</th>
                                <th>Date de fin</th>
                                <th>Statut</th>
                                <th>Salle</th>
                                <th>Etablissement</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listSession.map(session =>
                                     
                                    <tr key={session.idSession} >
                                        <td>{session.nomination}</td>
                                        <td>{session.dateDebut}</td>
                                        <td>{session.dateFin}</td>
                                        <td>{session.statut.nomination}</td>
                                        <td>{session.salle.nom}</td>
                                        <td>{session.etablissement.nom}</td>
                                        {/* <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherSession(session.idSession)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerSession(session.idSession)}>Supprimer</Button>
                                        </td> */}
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                 </Card>  
            </Container>
        )
    }
}
