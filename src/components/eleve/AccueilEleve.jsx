import React, { Component } from 'react';
import { Button, Card, Container, Typography } from '@material-ui/core';
import './../../css/service-eleve.css';

export default class AccueilEleve extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeEleve: [],
     }

        this.listeEleve = this.listeEleve.bind(this);
        this.creerEleve = this.creerEleve.bind(this);
    }


    listeEleve = () => {

        this.props.history.push("/liste-eleves");
    }

    creerEleve = () => {

        this.props.history.push("/ajouter-eleve");
    }


    render() {
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card className="eleves">
                    <Typography variant="h1">Eleves</Typography>
                    <Typography variant="h4">Vous avez actuellement n eleves</Typography>
                    <Button color="primary" variant="contained" onClick={() => { this.listeEleve() }}>Consulter</Button>
                    <Button color="primary" variant="contained" onClick={() => { this.creerEleve() }}>Creer</Button>
                </Card>
            </Container>
        )
    }
}
