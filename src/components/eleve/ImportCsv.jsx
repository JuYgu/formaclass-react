import React, { Component } from 'react'
import EleveService from '../../services/eleve/EleveService';
import './../../css/service-eleve.css';
import { Button, Card, Container, Typography } from '@material-ui/core';

export default class ImportCsv extends Component{

    state = {
        file : ''
    };
    
    componentDidMount = () => {
        const {file} = this.props;
        this.setState({ file })
    };
    
    uploadFile = ({target : {files}}) => {
        console.log(files[0]);
        let data = new FormData();
        data.append('file', files[0]);
        EleveService.postCsv(data);            
    };

    accueil = () => {
        this.props.history.push("/accueil-eleve");
    }
    
    render(){
        return(
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card>
                <div className="col-md-6">
                    <label>Importez votre fichier</label>
                    <Card>
                    <input type="file" onChange={this.uploadFile}/>
                    </Card>
                </div>
            </Card>
            <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
            </Container>
        )
    }
}