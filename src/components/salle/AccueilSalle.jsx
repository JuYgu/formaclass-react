import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import './../../css/service-salle.css';

export default class AccueilSalle extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeSalle: [],
     }

        this.listeSalle = this.listeSalle.bind(this);
        this.creerSalle = this.creerSalle.bind(this);
    }


    listeSalle = () => {

        this.props.history.push("/liste-salles");
    }

    creerSalle = () => {

        this.props.history.push("/ajouter-salle");
    }


    render() {
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card className="salles">
                    <Typography variant="h1">Salles</Typography>
                    <Typography variant="h4">Vous avez actuellement n salles</Typography>
                    <Button color="primary" variant="contained" onClick={() => { this.listeSalle() }}>Consulter</Button>
                    <Button color="primary" variant="contained" onClick={() => { this.creerSalle() }}>Creer</Button>
                </Card>
            </Container>
        )
    }
}
