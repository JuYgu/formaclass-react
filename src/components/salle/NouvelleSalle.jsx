import React, { Component } from 'react'
import { Box, Button, Card, CardContent, Container, Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Formik} from 'formik';
import FormikField from '../reusableComponent/formikField/Input';
import SalleService from '../../services/salle/SalleService';
import './../../css/service-salle.css';



export default class NouvelleSalle extends Component {

    constructor(props) {
        super(props);

        this.state = {
            idSalle: this.props.match.params.id,
            nom: '',
            capaciteMax: '',
            accessibilite: false,
            reservable: false,
            disponible: false,
            materiel: [],
            requeteOk: false,
            isLoaded: false,
        }
    }



    ajoutSalleSchema = Yup.object().shape({
        nom: Yup.string().min(3, "Minimum 3 caractère").max(20, "Maximum 20 caracteres").required("Veuillez indiquer le libelle de la salle"),
        capaciteMax: Yup.string().min(1, "indiquez une capacité de salle minimale").max(4).required("Veuillez indiquer la capacité maximale de cette salle"),
        accessibilite: Yup.boolean().oneOf([true, false], "Veuillez indiquer si cette salle est accessible aux personnes en situation de handicap"),
        reservable: Yup.boolean().oneOf([true, false], "Veuillez indiquer si cette salle pourra être réservée")
    })

    initialValues = {

        nom: "",
        capaciteMax: "",
        accessibilite: "",
        reservable: ""
    }

    handleSubmit = (values) => {

        console.log(values.nom, values.capaciteMax, values.accessibilite, values.reservable)
        SalleService.addSalle(values.nom, values.capaciteMax, values.accessibilite, values.reservable);
        this.props.history.push("/liste-salles");
        this.window.location.reload();
        
    }

    accueil = () => {
        this.props.history.push("/accueil-salle");
    }

    listeSalle = () => {
        this.props.history.push("/liste-salles");
    }

    render() {
        return (

            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
                <Card className="addSalle">
                    <Typography variant="h4">Ajouter une salle</Typography>
                    <CardContent className="container">
                        <Formik initialValues={this.initialValues}
                            onSubmit={this.handleSubmit}
                            validationSchema={this.ajoutSalleSchema}
                        >
                            <Form>
                                <Box paddingBottom={1}>
                                    <FormikField label="Nom" name="nom" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Capacite" name="capaciteMax" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Accessibilité" name="accessibilite" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Réservable" name="reservable" />
                                </Box>
                                <div className="button">
                                    <Button variant="contained" color="primary" type="submit" >
                                        Valider
                                    </Button>
                                </div>
                            </Form>
                        </Formik>
                    </CardContent>
                    <Button color="primary" variant="contained" onClick={() => { this.accueil() }}>Accueil</Button>
                </Card>
            </Container>
        );
    }
}


