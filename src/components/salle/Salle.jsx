import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import SalleService from '../../services/salle/SalleService';
import './../../css/service-salle.css';


export default class Salle extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idSalle: this.props.match.params.id,
            nom: '',
            capaciteMax: '',
            accessibilite : false,
            reservable: false,
            disponible: false,
            materiel: [],
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        console.log("salle data " + this.state.idSalle)
        SalleService.getSalle(this.state.idSalle).then((response) => {

            console.log("salle data" + response)
            this.setState({ nom: response.data.nom,
                            capaciteMax: response.data.capaciteMax,
                            accessibilite : response.data.accessibilite,
                            disponible : response.data.disponible,
                            reservable : response.data.reservable
            });           
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    editSalle(id){
        this.props.history.push(`/editer-salle/${id}`);
    }

    supprimerSalle(id){

        SalleService.deleteSalle(id);
        this.props.history.push("/liste-salles");
        window.location.reload();
   
    }

    accueil = () => {
        this.props.history.push("/accueil-salle");
    }

    listeSalle = () => {
        this.props.history.push("/liste-salles");
    }

    render() {
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card className="login">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Capacite</th>
                                <th>Materiel</th>
                                <th>Disponible</th>
                                <th>reservable</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr key={this.state.idSalle} >
                                {console.log("log : "+ this.state.idSalle)}
                                <td>{this.state.nom}</td>
                                <td>{this.state.capaciteMax}</td>
                                <td>MATERIEL</td>
                                <td>{this.state.disponible}</td>{/* <td>if(`${salle.disponible}`? "oui" : "non");</td> */}
                                <td>{this.state.reservable}</td>{/* <td>if(`${salle.reservable}`? "oui" : "non");</td> */}
                                <td>        
                                    <Button color="secondary" variant="contained" onClick={() => this.supprimerSalle(this.state.idSalle)}>Supprimer</Button>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                    <Button color="primary" variant="contained" onClick={() => this.editSalle(this.state.idSalle)}>modifier</Button>
                    </Card>
            </Container>
        )
    }

}


