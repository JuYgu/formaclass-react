import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import SalleService from '../../services/salle/SalleService';
import './../../css/login.css';

export default class ListeSalle extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeSalle: [],
            listeSalleUpdate: [],
            idSalle: '',
            idEnvoyee: '',
            update: false,
            requeteOk: false,
            isLoaded: false,
        }

        this.afficherSalle = this.afficherSalle.bind(this);
        this.supprimerSalle = this.supprimerSalle.bind(this);
    }

    componentDidMount() {

        SalleService.listeSalles().then((response) => {

            this.setState({ listeSalle: response.data });
        });
    }

    componentDidUpdate(prevState) {

        if(prevState === this.state.listeSalle)

        SalleService.listeSalles().then((response) => {

            this.setState({ listeSalleUpdate: response.data });
            
            if(this.state.listeSalleUpdate !== this.state.listeSalle)

             return this.setState({ listeSalle: this.state.listeSalleUpdate });
            
        });
    }

    afficherSalle(id){

        this.props.history.push(`/salle/${id}`);
    }

    supprimerSalle(id){

        SalleService.deleteSalle(id).then(() =>
            
            this.componentDidUpdate(this.state.listeSalle));
        
    }

    accueil = () => {
        this.props.history.push("/accueil-salle");
    }

    render() {
        return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="login">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Capacite</th>
                                <th>Materiel</th>
                                <th>Disponible</th>
                                <th>reservable</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listeSalle.map(salle =>
                                        <tr key={salle.idSalle} >
                                        <td>{salle.nom}</td>
                                        <td>{salle.capaciteMax}</td>
                                        <td>MATERIEL</td>
                                        <td>{salle.disponible}</td>{/* <td>if(`${salle.disponible}`? "oui" : "non");</td> */}
                                        <td>{salle.reservable}</td>{/* <td>if(`${salle.reservable}`? "oui" : "non");</td> */}
                                        <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherSalle(salle.idSalle)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerSalle(salle.idSalle)}>Supprimer</Button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                    </Card>
            </Container>
        )
    }
}
