import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import ProfesseurService from '../../services/professeur/ProfesseurService';
import './../../css/service-professeur.css';

export default class FormationsProfesseur extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeProfesseur: [],
            idProfesseur: '',
            idEnvoyee: '',
            update: false,
            requeteOk: false,
            isLoaded: false,
        }

        this.afficherProfesseur = this.afficherProfesseur.bind(this);
        this.supprimerProfesseur = this.supprimerProfesseur.bind(this);
    }

    componentDidMount() {

        ProfesseurService.listeProfesseurs().then((response) => {

            this.setState({ listeProfesseur: response.data });
        });
    }

    componentDidUpdate(){

      console.log("coucou")
        
    }

    afficherProfesseur(id){

        this.props.history.push(`/professeur/${id}`);
    }

    supprimerProfesseur(id){

        ProfesseurService.deleteProfesseur(id);
        
    }

    accueil = () => {
        this.props.history.push("/accueil-professeur");
    }

    render() {
        return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="tableauProf">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Categorie</th>
                                <th>Actif</th>
                                <th>Nb de form réalisées</th>
                                <th>Spécialités</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listeProfesseur.map(professeur =>
                                    <tr key={professeur.idProfesseur} >
                                        <td>{professeur.nom}</td>
                                        <td>{professeur.actif}</td>
                                        <td>nb de formations</td>{/* <td>if(`${professeur.disponible}`? "oui" : "non");</td> */}
                                        <td>{professeur.specialite}</td>{/* <td>if(`${professeur.reservable}`? "oui" : "non");</td> */}
                                        <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherProfesseur(professeur.idProfesseur)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerProfesseur(professeur.idProfesseur)}>Supprimer</Button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                    </Card>
            </Container>
        )
    }
}
