import React, { Component, useState } from 'react'
import { Box, Button, Card, CardContent, Step, StepLabel, Stepper, Container, Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Formik} from 'formik';
import FormikField from '../reusableComponent/formikField/Input';
import ProfesseurService from '../../services/professeur/ProfesseurService';
import './../../css/login.css';
import CategoryService from '../../services/category/Category-service';


export default class NouveauProfesseur extends Component {

    constructor(props) {
        super(props);

        this.state = {
            idProfesseur: this.props.match.params.id,
            listeCategories: []
        }
    }

    ajoutProfesseurSchema = Yup.object().shape({
        nom: Yup.string().min(1, "Minimum 1 caractère").max(50, "Maximum 20 caracteres").required("Veuillez indiquer votre ville"),
        prenom: Yup.string().min(1, "indiquez une capacité de professeur minimale").max(50).required("Veuillez indiquer votre prénom"),
        voie: Yup.string().min(6, "Minimum 6 caractère").max(60, "Maximum 60 caracteres").required("Veuillez renseigner votre adresse"),
        ville: Yup.string().min(1, "Minimum 1 caractère").max(40, "Maximum 40 caracteres").required("Veuillez indiquer votre ville"),
        codePostal: Yup.string().matches(/^\d+$/).min(5, "veuillez indiquer les 5 chiffres de votre code postal").max(5).required("Veuillez indiquer votre code postal"),
        telephone: Yup.string().matches(/^\d+$/).min(10, "veuillez indiquer les 10 chiffres de votre numéro de téléphone").max(10).required("Veuillez indiquer votre numéro de téléphone"),
    })

    categorieSchema = Yup.object().shape({
        categorie: Yup.object().shape({

           
        })
    })

    authUserSchema = Yup.object().shape({
        authUser: Yup.object().shape({
            email: Yup.string().email().required("Veuillez indiquer votre email"),
            password: Yup.string().min(7, "Minimum 7 caracteres").max(20, "Maximum 20 caracteres").required("Veuillez indiquer votre email"),
        })
    })

    initialValues = {

        nom: "",
        prenom: "",
        voie: "",
        ville: "",
        codePostal: "",
        telephone: "",
        categorie:{
            type: "",
        },
        authUser: {
            email: "",
            password: "",
            checkpassword: ""
        }
    }

    componentDidMount() {

        CategoryService.getAllCategory().then((response) => {

            this.setState({ listeCategories: response.data })
        });
    }

    handleSubmit = (values) => {

        ProfesseurService.addProfesseur(values.nom, values.prenom, values.voie, values.ville, values.codePostal, values.telephone, values.authUser);
        this.props.history.push("/liste-professeurs");
        this.window.location.reload();
    }

    accueil = () => {
        this.props.history.push("/accueil-professeur");
    }

    listeProfesseur = () => {
        this.props.history.push("/liste-professeurs");
    }

    render() {
        return (

            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
                <Card className="addProfesseur">
                    <Typography variant="h4">Ajouter un professeur</Typography>
                    <CardContent className="container">
                        <FormikStepper
                            initialValues={this.initialValues}
                            onSubmit={this.handleSubmit}
                        >

                            <FormikStep validationSchema={this.ajoutProfesseurSchema}>
                                <Box paddingBottom={1}>
                                    <FormikField label="Nom" name="nom" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Prenom" name="prenom" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="voie" name="voie" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Ville" name="ville" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Code postal" name="codePostal" />
                                </Box>
                                <Box paddingBottom={1}>
                                    <FormikField label="Telephone" name="telephone" />
                                </Box>
                                <div className="button">
                                    <Button variant="contained" color="primary" type="submit" >
                                        Valider
                                    </Button>
                                </div>
                            </FormikStep>

                            <FormikStep validationSchema={this.authUserSchema} label="Identifiants">
                               
                                <Box paddingBottom={1}>
                                    <select
                                        name="type"
                                        value={this.state.listeCategories.map(categorie => categorie.type)}
                                        style={{ display: 'block' }}
                                    >
                                        <option value="" label="Selectionner une catégorie" />
                                        {this.state.listeCategories.map(categorie =>

                                            <option value={categorie.type} label={categorie.type}/>
                                                )
                                        }
                                   </select>
                                </Box>

                                    <Box paddingBottom={1}>
                                        <FormikField label="Email" name={`authUser.email`} type="email" />
                                        {/* <p className="errorMessage">{this.state.errorUserNameMessage}</p> */}
                                    </Box>
                                    <Box paddingBottom={1}>
                                        <FormikField label="Password" name={`authUser.password`} type="password" />
                                    </Box>
                            </FormikStep>
                        </FormikStepper>

                    </CardContent>
                        <Button color="primary" variant="contained" onClick={() => { this.accueil() }}>Accueil</Button>
                </Card>
            </Container>
        );
    }
}

export function FormikStep({ children}) {
    return <>{children}</>
}

export function FormikStepper({ children, errorName, ...props }) {



    const childrenArray = React.Children.toArray(children);
    const [step, setStep] = useState(0);
    const currentChild = childrenArray[step];

    //A voir si on l'utilise... DOit normalement servir a disabled le bouton submit
    // var errorEtabName = {errorName}



    function isLastStep() {
        return step === childrenArray.length - 1;
    }
    return (
                <>
                    <Typography variant="h3">Inscription</Typography>
                    <Formik
                        {...props}
                        validationSchema={currentChild.props.validationSchema}
                        onSubmit={async (values, helpers) => {
                            // Faire ici la vérif du nom de l'établissement
                            if (isLastStep()) {
                                await props.onSubmit(values, helpers);
                            } else {
                                setStep(s => s + 1);
                            }
                        }}>
                        {({ isSubmitting, dirty, isValid }) => (
                            <Form autoComplete="off">
                                <Stepper alternativeLabel activeStep={step}>
                                    {childrenArray.map((child) => (
                                        <Step key={child.props.label}>
                                            <StepLabel>{child.props.label}</StepLabel>
                                        </Step>
                                    ))}
                                </Stepper>

                                {currentChild}
                                {step > 0 ? (
                                    <Button disabled={isSubmitting} onClick={() => setStep(s => s - 1)} variant="contained" color="primary">Retour</Button>
                                ) : null}

                                <Button disabled={!dirty || !isValid || isSubmitting} variant="contained" color="primary" type="submit">{isSubmitting ? 'Validation Formulaire' : isLastStep() ? 'Valider' : 'Suivant'}</Button>


                            </Form>
                        )}

                    </Formik>

                </>
    )

}