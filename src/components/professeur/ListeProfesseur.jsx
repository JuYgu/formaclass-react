import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import ProfesseurService from '../../services/professeur/ProfesseurService';
import './../../css/login.css';

export default class ListeProfesseur extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeProfesseur: [],
            idProfesseur: '',
            idEnvoyee: '',
            update: false,
            requeteOk: false,
            isLoaded: false,
        }

        this.afficherProfesseur = this.afficherProfesseur.bind(this);
        this.supprimerProfesseur = this.supprimerProfesseur.bind(this);
    }

    componentDidMount() {

        ProfesseurService.listeProfesseurs().then((response) => {

            this.setState({ listeProfesseur: response.data });
        });
    }

    componentDidUpdate(prevState) {

        if(prevState === this.state.listeProfesseur)

        ProfesseurService.listeProfesseurs().then((response) => {

            this.setState({ listeProfesseurUpdate: response.data });
            
            if(this.state.listeProfesseurUpdate !== this.state.listeProfesseur)

             return this.setState({ listeProfesseur: this.state.listeProfesseurUpdate });
            
        });
    }

    afficherProfesseur(id){

        this.props.history.push(`/professeur/${id}`);
    }

    supprimerProfesseur(id){

        ProfesseurService.deleteProfesseur(id).then(() =>
            
        this.componentDidUpdate(this.state.listeProfesseur));
        
    }

    accueil = () => {
        this.props.history.push("/accueil-professeur");
    }

    render() {

     return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="login">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Actif</th>
                                <th>Categorie</th>
                                <th>Spécialités</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listeProfesseur.map(professeur =>
                                     
                                    <tr key={professeur.idProfesseur} >
                                        <td>{professeur.nom}</td>
                                        <td>{professeur.actif === true ? 'oui' : 'non'}</td>
                                        {/* <td>{professeur.categorie.type}</td> */}
                                        <td>{professeur.specialite}</td>
                                        <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherProfesseur(professeur.idProfesseur)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerProfesseur(professeur.idProfesseur)}>Supprimer</Button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                 </Card>  
            </Container>
        )
    }
}
