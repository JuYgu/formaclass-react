import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import ProfesseurService from '../../services/professeur/ProfesseurService';
import './../../css/service-professeur.css';


export default class Professeur extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idProfesseur: this.props.match.params.id,
            nom: '',
            prenom: "",
            voie: "",
            ville: "",
            codePostal: "",
            telephone: "",
            specialite : "",
            actif : true,
            type: "",
            email: "",
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        console.log("professeur data " + this.state.idProfesseur)
        ProfesseurService.getProfesseur(this.state.idProfesseur).then((response) => {

            console.log("professeur data" + response)
            this.setState({ nom: response.data.nom,
                            prenom : response.data.prenom,
                            voie : response.data.voie,
                            ville : response.data.ville,
                            codePostal : response.data.codePostal,
                            telephone : response.data.telephone,
                            actif : response.data.actif,
                            //type : response.data.categorie.type,
                            email : response.data.authUser.email,
                            specialite : response.data.specialite
            });           
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    editProfesseur(id){
        this.props.history.push(`/editer-professeur/${id}`);
    }

    supprimerProfesseur(id){

        ProfesseurService.deleteProfesseur(id);
        this.listeProfesseur();
        this.window.location.reload();
    }

    accueil = () => {
        this.props.history.push("/accueil-professeur");
    }

    listeProfesseur = () => {
        this.props.history.push("/liste-professeurs");
    }

    render() {

        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card>
                    
                            <tr className ="login" key={this.state.idProfesseur} >
                                <td> Nom : {this.state.nom}</td>
                                <td> Prénom : {this.state.prenom}</td>
                                <td> Mail : {this.state.email}</td>
                                <td> Adresse :  {this.state.voie}  {this.state.codePostal}  {this.state.ville}</td>
                                <td> Téléphone : {this.state.telephone}</td>
                                <td> Catégorie : {this.state.type}</td>
                                <td> Spécialités : {this.state.specialite}</td>  
                                <td>  
                                <Button color="primary" variant="contained" onClick={() => this.editProfesseur(this.state.idProfesseur)}>modifier</Button>
                                </td>
                            </tr>
                    </Card>
                    <Card>
                    
                            <tr className ="login" key={this.state.idProfesseur} >
                                <td> Actif : {this.state.actif === true ? 'oui' : 'non'}</td>
                                {/* <td> Form réalisée : count sur la table session join avec la table professeur afin de recuperer les sessions par l'id professeur</td> */}
                                {/* <td> Form en cours : recherche sur la table session join avec la table professeur afin de recuperer les sessions par l'id professeur join avec la table statut pour récuperer celle au statut "en cours"</td> */}
                            </tr>
                   </Card>
                <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
            </Container>
        )
    }

}


