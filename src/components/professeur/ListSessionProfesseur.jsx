import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import ProfesseurService from '../../services/professeur/ProfesseurService';
import './../../css/service-professeur.css';


export default class ListSessionProfesseur extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idProfesseur: this.props.match.params.id,
            listSession : [],
            nomination : "",
            dateDebut: "",
            dateFin: "",
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        ProfesseurService.getListSessionProfesseur(this.state.idProfesseur).then((response) => {

            console.log("professeur data" + response)
            this.setState({ listSession : response.data});  
                     
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    accueil = () => {
        this.props.history.push("/accueil-professeur");
    }

    listeProfesseur = () => {
        this.props.history.push("/liste-professeurs");
    }

    render() {

        return (
            <Container>
            <Typography variant="h1">FORMACLASS</Typography>
            <Card className="login">
                <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nomination</th>
                                <th>Date début</th>
                                <th>Date de fin</th>
                                <th>Statut</th>
                                <th>Salle</th>
                                <th>Etablissement</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.listSession.map(session =>
                                     
                                    <tr key={session.idSession} >
                                        <td>{session.nomination}</td>
                                        <td>{session.dateDebut}</td>
                                        <td>{session.dateFin}</td>
                                        <td>{session.statut.nomination}</td>
                                        <td>{session.salle.nom}</td>
                                        <td>{session.etablissement.nom}</td>
                                        {/* <td>
                                            <Button color="secondary" variant="contained" onClick={() => this.afficherSession(session.idSession)}>afficher</Button>
                                            <Button color="secondary" variant="contained" onClick={() => this.supprimerSession(session.idSession)}>Supprimer</Button>
                                        </td> */}
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                 </Card>  
            </Container>
        )
    }
}
