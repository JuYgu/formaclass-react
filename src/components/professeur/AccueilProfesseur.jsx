import React, { Component } from 'react';
import { Button, Card, Container, Typography } from '@material-ui/core';
import './../../css/service-professeur.css';

export default class AccueilProfesseur extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listeProfesseur: [],
     }

        this.listeProfesseur = this.listeProfesseur.bind(this);
        this.creerProfesseur = this.creerProfesseur.bind(this);
    }


    listeProfesseur = () => {

        this.props.history.push("/liste-professeurs");
    }

    creerProfesseur = () => {

        this.props.history.push("/ajouter-professeur");
    }


    render() {
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card className="professeurs">
                    <Typography variant="h1">Professeurs</Typography>
                    <Typography variant="h4">Vous avez actuellement n professeurs</Typography>
                    <Button color="primary" variant="contained" onClick={() => { this.listeProfesseur() }}>Consulter</Button>
                    <Button color="primary" variant="contained" onClick={() => { this.creerProfesseur() }}>Creer</Button>
                </Card>
            </Container>
        )
    }
}
