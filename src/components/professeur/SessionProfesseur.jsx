import React, { Component } from 'react'
import { Button, Card, Container, Typography } from '@material-ui/core';
import ProfesseurService from '../../services/professeur/ProfesseurService';
import './../../css/service-professeur.css';


export default class SessionProfesseur extends Component {

    constructor(props) {
        super(props);


        this.state = {
            idProfesseur: this.props.match.params.id,
            nomination :"",
            dateDebut: "",
            dateFin: "",
            requeteOk: false,
            isLoaded: false,
        }
    }

    componentDidMount() {

        console.log("professeur data " + this.state.idProfesseur)
        ProfesseurService.getSessionProfesseur(this.state.idProfesseur).then((response) => {

            console.log("professeur data" + response)
            this.setState({ nomination: response.data.nomination,
                            dateDebut : response.data.dateDebut,
                            dateFin : response.data.dateFin,
            });           
        }, error => {
            this.setState({ requeteOk: false, isLoaded: true })
        });
    }

    accueil = () => {
        this.props.history.push("/accueil-professeur");
    }

    listeProfesseur = () => {
        this.props.history.push("/liste-professeurs");
    }

    render() {

    
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card>
                    
                            <tr className ="login" key={this.state.idProfesseur} >
                                <td> Nomination : {this.state.nomination}</td>
                                <td> Date début : {this.state.dateDebut}</td>
                                <td> Date fin : {this.state.dateFin}</td>
                            </tr>
                    </Card>
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
            </Container>
        )
    }

}


