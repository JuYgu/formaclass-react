import React, { Component } from 'react';
import { Box, Button, Card, CardContent, Container, Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Formik } from 'formik';
import FormikField from '../reusableComponent/formikField/Input';
import EquipementService from '../../services/equipement/EquipementService';
import './../../css/service-equipement.css';

export default class Equipement extends Component {

    constructor(props){
        super(props);

        this.state = {
            idEquipement: this.props.match.params.id,
            nom: '',
            quantiteTotale: ''
        }
    }

    ajouterEquipementSchema = Yup.object().shape({
        nom: Yup.string().min(4, "3 caractères minimum").max(20, "20 caractères maximum").required("Indiquez le nom complet de l'équipement à enregistrer"),
        quantiteTotale: Yup.string().min(1, "Veuillez indiquer la quantité à ajouter de ce matériel").max(5)
    })

    initialValues = {
        nom: '',
        quantiteTotale: ''
    }

    handleSubmit = (values) => {

        EquipementService.addEquipement(values.nom, values.quantiteTotale);
        this.props.history.push("/liste-equipements");
        this.window.location.reload();
    }

    accueil = () => {

        this.props.history.push("/accueil-equipement");
    }

    listerEquipement = () => {
        this.props.history.push("/liste-equipements");
    }

    render() {
        return(

            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
                <Card className="addEquipement">
                    <Typography variant="h4">Ajouter un equipement</Typography>
                    <CardContent className="container">
                        <Formik initialValues={ this.initialValues } onSubmit={ this.handleSubmit } validationSchema={ this.ajouterEquipementSchema }>
                            <Form>
                                <Box paddingBottom={ 1 }>
                                    <FormikField label="Nom" name="nom"/>
                                </Box>
                                <Box paddingBottom={ 1 }>
                                    <FormikField label="Quantite totale" name="quantiteTotale"/>
                                </Box>
                                <div className="button">
                                    <Button variant="contained" color="primary" type="submit">Valider</Button>
                                </div>
                            </Form>
                        </Formik>
                    </CardContent>
                    <Button color="primary" variant="contained" onClick={ () => { this.accueil() } }>Accueil</Button>
                </Card>
            </Container>
        );
    }

}