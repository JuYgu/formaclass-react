import React, { Component } from 'react';
import { Button, Card, Container, Typography } from '@material-ui/core';
import './../../css/service-equipement.css';

export default class AccueilEquipement extends Component{

    constructor(props){
        super(props);

        this.state = {
            listeEquipement: []
        }

        this.listeEquipement = this.listeEquipement.bind(this);
        this.creerEquipement = this.creerEquipement.bind(this);
    }

    listeEquipement = () => {
        this.props.history.push("/liste-equipements");
    }

    creerEquipement = () => {
        this.props.history.push("/ajouter-equipement");
    }

    render(){
        return(
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card className="equipements">
                    <Typography variant="h1">Equipements</Typography>
                    <Typography variant="h4">x equipements ont ete ajoutes</Typography>
                    <Button color="primary" variant="contained" onClick={ () => { this.listerEquipement() } }>Consulter</Button>
                    <Button color="primary" variant="contained" onClick={ () => { this.creerEquipement() } }>Creer un equipement</Button>
                </Card>
            </Container>
        );
    }

}