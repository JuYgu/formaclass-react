import React, { Component } from 'react'
import { Card, Container, Typography, Button, Box } from '@material-ui/core';
import CategoryService from '../../services/category/Category-service';
import './session.css'
export default class AccueilServiceFormation extends Component {
    constructor(props) {
        super(props)

        this.state = {
            completeFormation : [],
            incompleteFormation : [],
            modele : [],
            categories : []

        }
    }

    creationModeleClick = () => {
        CategoryService.getAllCategory()
                        .then( response => {
                            this.props.history.push({
                                pathname: "/modeles/new",
                                state: { categories : response.data}
                            });
                        })
    }

    render() {
        return (
            <Container className="container">
                <Typography variant="h1">Formations</Typography>

                <Box className="box">
                    <Card className="card">
                        <Typography variant="h4"> Créer Formation</Typography>
                        <div className="button">
                            <Button variant="contained" color="primary" className="buttonHover" onClick={() => { this.props.history.push("/sessions/new")}}> Creer Formation</Button>
                            <Button variant="contained" color="primary" className="buttonHover" onClick={() => {this.creationModeleClick()}}> Creer Modele</Button>
                        </div>
                    </Card>
                    <Card className="card">
                        <Typography variant="h4"> Consulter Formation</Typography>
                        <div className="button">
                            <Button variant="contained" color="primary" className="buttonHover" onClick={() => { this.props.history.push("/sessions/complete")}}> Formations Complètes</Button>
                            <Button variant="contained" color="primary" className="buttonHover" onClick={() => { this.props.history.push("/sessions/incomplete")}}> Formations incompletes</Button>
                            <Button variant="contained" color="primary" className="buttonHover" onClick={() => { this.props.history.push("/modeles")}}> Modèles</Button>
                        </div>
                    </Card>
                    <Card className="card">
                        <Typography variant="h4"> Categories</Typography>
                        <div className="button">
                            <Button variant="contained" color="primary" onClick={() => { this.props.history.push("/categories")}}> Consulter</Button>
                            <Button variant="contained" color="primary"  onClick={() => { this.props.history.push("/categories/new")}}> Creer</Button>
                        </div>
                    </Card>
                </Box>
            </Container>
        )
    }
}
