import React, {useState} from 'react'
import {useLocation, useHistory} from 'react-router-dom'
import { Card, Typography, Button, IconButton, makeStyles } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete'
import '../../session.css'
import VisibilityIcon from '@material-ui/icons/Visibility';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import SessionService from '../../../../services/session/Session-service'
import SallesByCriteria from '../recherches/SallesByCriteria';
import PopUp from '../../../reusableComponent/popUp/PopUp';
import ProfesseursyCriteria from '../recherches/ProfesseursByCriteria';
import ElevesByCriteria from '../recherches/ElevesByCriteria';
import ViewEleves from '../recherches/ViewEleves';


const useStyles = makeStyles(theme => ({
    buttonSee: {
            backgroundColor: "#0c4622",
            cursor: 'pointer'
    },
    buttonAdd: {
        backgroundColor: "#beae6c",
        cursor: 'pointer'
    },
    containerCard : {
        display: "flex",
        margin: "0 45px 45px 45px"
    },
    colorGreen: {
        backgroundColor : "#0e714761",
        padding: "15px",
        marginRight: "25px"
    },
    typoText : {
        color: "white",
        display: "inline-block",
        width: "140px",
        textAlign: "right",
    }
}))
export default function SessionView(props) {

    const classes = useStyles();
    const location = useLocation();
    const history = useHistory();

    const [session, setSession] = useState(location.state.session) 


    const [openPopupSalle, setOpenPopupSalle] = useState(false)
    const [openPopupProfesseur, setOpenPopupProfesseur] = useState(false)
    const [openPopupEleves, setOpenPopupEleves] = useState(false)
    const [openPopupElevesList, setOpenPopupElevesList] = useState(false)


    
    const criteriaSalle =  {
        debutSession : location.state.session.dateDebut,
        finSession : location.state.session.dateFin,
        accessibilite : true,
        capaciteMax : location.state.session.modele.eleveMax
    }
    const criteriaProfesseur =  {
        idCategorie : location.state.session.modele.categorie.idCategorie,
        dateDebut : location.state.session.dateDebut,
        dateFin : location.state.session.dateFin
    }
    const criteriaEleves =  {
        idModele : location.state.session.modele.idModele,
        dateDebut : location.state.session.dateDebut,
        dateFin : location.state.session.dateFin
    }


    //AJOUT SUPPRESSION SALLE
    const editForSalle =  async (salle) => {
        //SalleService.findSallesByCriteria(criteriaSalle.debutSession, criteriaSalle.finSession, criteriaSalle.accessibilite, criteriaSalle.capaciteMax)
        let newSession = await SessionService.addSalle(session.idSession, salle).then(response => {return response.data});
        await setSession(newSession);
        setOpenPopupSalle(false)

    }
    const openInPopupSalle = ()  => {
        setOpenPopupSalle(true)
    }

    const deleteSalle = async () => {
        let newSession = await SessionService.deleteSalle(session.idSession).then(response => {return response.data})
        await setSession(newSession);
    }

    // AJOUT SUPPRESSION PROFESSEUR
    const editForProfesseur = async (professeur) => {
        let newSession = await SessionService.addProfesseur(session.idSession, professeur).then(response => {return response.data});
        await setSession(newSession);
        setOpenPopupProfesseur(false)
    }
    const openInPopupProfesseur = ()  => {
        setOpenPopupProfesseur(true)
    }
    const deleteProfesseur = async () => {
        let newSession = await SessionService.deleteProfesseur(session.idSession).then(response => {return response.data})
        await setSession(newSession);

    }

    // AJOUT ELEVE
    const editForEleves = async (eleves) => {
        let newSession = await SessionService.addEleves(session.idSession, eleves).then(response => {return response.data});
        console.log("session de vant contenur des eleves", newSession)
        await setSession(newSession);
        setOpenPopupEleves(false)
    }
    const openInPopupEleves = ()  => {
        setOpenPopupEleves(true)
    }

    // VIEW EleveList
    const openInPopupElevesList = ()  => {
        setOpenPopupElevesList(true)
    }

    const deleteForEleve = async (idEleve) => {
        let newSession = await SessionService.deleteEleves(session.idSession, idEleve).then(response => {return response.data});
        await setSession(newSession);
        
    }




    let returnElevePossibility;
        if (session.listeEleve == null) {returnElevePossibility = <Button onClick={() => { openInPopupEleves()}}> Ajouter Eleves</Button>}
        if (session.listeEleve.length < session.modele.eleveMin && session.listeEleve != null) { returnElevePossibility = <> <span>{session.listeEleve.length}</span> <Button  onClick={ () => { openInPopupEleves()}} className={classes.buttonAdd}> <AddCircleIcon/> Ajouter Eleves</Button>  <Button onClick={ () => { openInPopupElevesList()}} className={classes.buttonSee}> <VisibilityIcon/>  Voir liste Eleves</Button></>}
        if (session.listeEleve.length === session.modele.eleveMax ) { returnElevePossibility =  <> <Button onClick={ () => { openInPopupElevesList()}}></Button> <Button className={classes.buttonSee}> Voir liste Eleves</Button> </>}
        
    
    return (
        <Card className="containerCard">
            <Typography variant="h1"> Formation </Typography>
            <div className={classes.containerCard}>
                <Card className={classes.colorGreen}>
                    <div className="text"><p>Type :</p>  <Typography className={classes.typoText}> {session.modele.titre} </Typography> </div>
                    <div className="text"><p>Libelle :</p> <Typography  className={classes.typoText}> {session.nomination} </Typography> </div>
                    <div className="text"><p>Date debut :</p>    <Typography  className={classes.typoText}> {session.dateDebut} </Typography> </div>
                    <div className="text"><p>Date fin :</p>    <Typography  className={classes.typoText}> {session.dateFin} </Typography> </div>
                    <div className="text"><p>Nombre eleves min :</p> <Typography  className={classes.typoText}> {session.modele.eleveMin} </Typography> </div>
                    <div className="text"><p>Nombre eleves max :</p>  <Typography  className={classes.typoText}> {session.modele.eleveMax} </Typography> </div>
                    <div className="text"><p style={{alignSelf: "flex-start"}}>Description : </p> 
                        <Card className="description">
                            <Typography > {session.modele.description} </Typography> 
                        </Card>
                        
                    </div>
                </Card>
                <Card className={classes.colorGreen}>
                    <div className="text"> <p> Salle : </p>
                        {
                            session.salle == null 
                            ? <Button onClick={() => {openInPopupSalle()}}> Ajouter Salle</Button>
                            : <> <Typography className={classes.typoText}> {session.salle.nom}  </Typography>  <IconButton onClick={() => {deleteSalle()}}> <DeleteIcon/> </IconButton> </>
                        }
                        <PopUp
                            title = {`Ajouter Salle`}
                            openPopup = {openPopupSalle}
                            setOpenPopup = {setOpenPopupSalle}
                        >
                            <SallesByCriteria criteriaSalle={criteriaSalle} setOpenPopupSalle={setOpenPopupSalle}  editForSalle={editForSalle} />
                        </PopUp>
                        
                    </div>
                    <div className="text"> <p>Professeur : </p>
                        {
                            session.professeur == null 
                            ? <Button onClick={() => {openInPopupProfesseur()}}> Ajouter Professeur</Button>
                            : <> <Typography className={classes.typoText}> {session.professeur.nom} {session.professeur.prenom} </Typography> <IconButton onClick={() => {deleteProfesseur()}}> <DeleteIcon/> </IconButton>    </>
                        }

                        <PopUp
                            title = {`Ajouter Professeur`}
                            openPopup = {openPopupProfesseur}
                            setOpenPopup = {setOpenPopupProfesseur}
                        >
                            <ProfesseursyCriteria criteriaProfesseur={criteriaProfesseur} setOpenPopupProfesseur={setOpenPopupProfesseur}  editForProfesseur={editForProfesseur} />

                        </PopUp>
                        
                    </div>
                    <div className="text"><p> Eleves : </p>
                        <div>
                            {returnElevePossibility }
                        </div>

                        <PopUp
                            title = {`Ajouter Eleves`}
                            openPopup = {openPopupEleves}
                            setOpenPopup = {setOpenPopupEleves}
                        >
                            <ElevesByCriteria criteriaEleves={criteriaEleves}  editForEleves={editForEleves} />

                        </PopUp>

                        <PopUp
                            title = {`Eleves inscrits à la formation`}
                            openPopup = {openPopupElevesList}
                            setOpenPopup = {setOpenPopupElevesList}
                        >
                            <ViewEleves openPopupElevesList={openPopupElevesList} eleves={session.listeEleve} deleteForEleve={deleteForEleve}  />
                        </PopUp>
                        
                    </div>
                </Card>
            </div>
            
                <Button onClick={() => { history.goBack()}}> retour</Button>
        </Card>
    )
}
