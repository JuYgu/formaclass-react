import React ,{ useState, useEffect } from 'react'
import {useHistory} from 'react-router-dom'
import SessionService from '../../../../services/session/Session-service'

import { Card, Typography, Toolbar, TableContainer, Paper, Button, makeStyles } from '@material-ui/core';
import SessionsList from './SessionsList';
import SearchBar from '../../../reusableComponent/seachBar/searchBar';


const useStyles = makeStyles(theme => ({
    table: {
        '& thead th': {
            fontWeight: '600',
            color : theme.palette.primary.light,
            backgroundColor: theme.palette.primary
        },
        '& tbody tr:hover': {
            backgroundColor: "#ecd98e",
            cursor: 'pointer'
        }
    },
    button:{
        root: {
            backgroundColor : theme.palette.primary
        }
    }
}))

const headCells = [
    "Libelle", "Description", "Categorie", "Date Début", "Date Fin", "Nombre d'eleves", "Statut", "Action"
]

export default function SessionsComplete() {
    const classes = useStyles();
    const [sessionsComplete, setSessionsComplete] = useState([]);
    const [search, setSearch] = useState('');
    const history = useHistory()

    const title = "Formations";

    useEffect(  () => {
        SessionService.getCompleteSession()
                        .then( response => {

                            setSessionsComplete(response.data)

                        })
    }, [])


   const handleSearch = (e) => {
        setSearch(e.target.value)
        console.log(e.target.value)
   }

    
    return (
        <Card>
        <Typography variant="h3"> Formations </Typography>
        <Toolbar>
           <SearchBar handleSearch={handleSearch} />
            {/* <Button variant="contained" startIcon = {<AddIcon/>} onClick={() => this.props.history.push("/modeles/new")}>Ajouter Modèle</Button> */}
        </Toolbar>
        <TableContainer component={Paper}>
           <SessionsList sessions={sessionsComplete} title={title} headCells={headCells} search={search}/>
        </TableContainer>
        <Button className={classes.button} onClick={() => { history.push("/accueil-formations")}} variant="contained"> retour</Button>
    </Card>
    )
}
