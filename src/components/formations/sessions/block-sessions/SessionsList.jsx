import React, {useState} from 'react'
import {useHistory} from 'react-router-dom'
import { Table, TableHead, TableRow, TableCell, TableBody, TablePagination, IconButton, makeStyles } from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';



const useStyles = makeStyles(theme => ({
    table: {
        '& thead th': {
            fontWeight: '600',
            color : "white",
            backgroundColor: theme.palette.primary
        },
        '& tbody tr:hover': {
            backgroundColor: "#ecd98e",
            cursor: 'pointer'
        },
        '& thead' : {
            backgroundColor: theme.palette.primary
        }
    },
    TableHead: {
        backgroundColor: "#104654",
        color: "white"
    },
    button:{

        margin: "15px",
        '&:hover':{
            backgroundColor: "#1b5e95",
        },
        root : {
            margin: "15px"
        }        
    },
    toolbar: {
        marginBottom: "10px"
    }
}))
export default function SessionsList(props) {

    const classes = useStyles();
    const history = useHistory();
    const headCells = props.headCells;
    const sessions = props.sessions;
    const search = props.search;

    let pages = [ 5 , 10,  25];
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

    const sessionDisplaying = () => { return sessions.filter((s) => s.nomination.toLowerCase().includes(search))}

    const handleChangePage = (event, newPage) => { setPage(newPage)}
    
    const handleChangeRowsPerPage = (e) => {
        setRowsPerPage(parseInt(e.target.value,10)); 
        setPage(0)}

    const sessionAfterPagingAndSorting = () => {return sessionDisplaying().slice( page * rowsPerPage, (page+1) * rowsPerPage)}

    const goViewSession = (session) => {
        console.log(history)
        history.push({
            pathname : "/sessions/session",
            state: {session}
        });
    }

    


    //const search = props.search;

    return (
        <>
        <Table className={classes.table}>
               <TableHead className={classes.TableHead}>
                   <TableRow>
                       {
                           headCells.map(head => (
                               <TableCell> {head} </TableCell>   
                           ))
                       }
                   </TableRow>
               </TableHead>
                   <TableBody>
                   {
                       sessionAfterPagingAndSorting().map( session => (
                           <TableRow key={session.idSession} onClick={() => {goViewSession(session)}}>
                                
                                <TableCell>{session.nomination}</TableCell>
                                <TableCell>{session.modele.description}</TableCell>
                                <TableCell>{session.modele.categorie.type}</TableCell>
                                <TableCell>{session.dateDebut}</TableCell>
                                <TableCell>{session.dateFin}</TableCell>
                                <TableCell>{session.listeEleve.length} sur {session.modele.eleveMin}</TableCell>
                                <TableCell>{session.statut.nomination.toLowerCase()}</TableCell>
                                <TableCell>
                                    {/* <IconButton   onClick= {() => {openInPopup()}}  >
                                        <EditOutinedIcon color="primary"  />
                                    </IconButton> */}
                                    <IconButton onClick={() => {goViewSession(session)}} >
                                        <VisibilityIcon color="primary"/>
                                    </IconButton>
                                    {/* <IconButton onClick={() => {props.deleteModele(idModele)}} >
                                        <CloseIcon color="primary"/>
                                    </IconButton> */}
                                </TableCell>

                                {/* <PopUp
                                    title = {`Modification ${titre}`}
                                    openPopup = {openPopup}
                                    setOpenPopup = {setOpenPopup}
                                >
                                    <UpdateModele modele = {props.modele} categories = {props.categories} editModele={props.editModele} openPopup={openPopup} setOpenPopup={setOpenPopup} />

                                </PopUp> */}
                                
                            


                           </TableRow>
                       ))
                   }
                   </TableBody>
                   <TablePagination
                           page = {page}
                           rowsPerPageOptions={pages}
                           rowsPerPage={rowsPerPage}
                           count={sessions.length}
                           onChangePage = {handleChangePage}
                           onChangeRowsPerPage = {handleChangeRowsPerPage }
                           color="primary"
                   />
           </Table>  
   </>
    )
}
