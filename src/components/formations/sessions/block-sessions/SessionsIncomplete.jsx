import React ,{ useState, useEffect } from 'react'
import {useHistory} from 'react-router-dom'
import SessionService from '../../../../services/session/Session-service';
import { Card, Typography, Toolbar, TableContainer, Paper, Button, makeStyles } from '@material-ui/core';
import SessionsList from './SessionsList';
import SearchBar from '../../../reusableComponent/seachBar/searchBar';


const headCells = [
    "Libelle", "Description", "Categorie", "Date Début", "Date Fin", "Nombre d'eleves", "Statut", "Action"
]


const useStyles = makeStyles(theme => ({
    table: {
        '& thead th': {
            fontWeight: '600',
            color : theme.palette.primary.light,
            backgroundColor: theme.palette.primary
        },
        '& tbody tr:hover': {
            backgroundColor: "#ecd98e",
            cursor: 'pointer'
        }
    },
    button:{
        margin: "15px",
        '&:hover':{
            backgroundColor: "#1b5e95",
        },
        root : {
            margin: "15px"
        }        
    },
    toolbar: {
        marginBottom: "10px",
        display: "flex",
        alignItems: "first baseline"
        
    },
    card : {
        padding: "0 25px"
    },
    title : {
        margin: "25px 0"
    },
    color : {
        
    }
}))

export default function SessionsIncomplete() {

    const [sessionsIncomplete, setSessionsIncomplete] = useState([]);
    const [search, setSearch] = useState('');
    const history = useHistory()
    const title = "Formations a Completer";
    const classes = useStyles();

    useEffect(  () => {
        SessionService.getIncompleteSession()
                        .then( response => {

                            setSessionsIncomplete(response.data)

                        })
    }, [])


   const handleSearch = (e) => {
        setSearch(e.target.value)
        console.log(e.target.value)
   }

    
    return (
        <Card className={classes.card}>
        <Typography variant="h3" className={classes.title}> Formation a Completer </Typography>
        <Toolbar className={classes.toolbar}>
           <SearchBar handleSearch={handleSearch} />
            {/* <Button variant="contained" startIcon = {<AddIcon/>} onClick={() => this.props.history.push("/modeles/new")}>Ajouter Modèle</Button> */}
        </Toolbar>
        <TableContainer component={Paper}>
           <SessionsList sessions={sessionsIncomplete} title={title} headCells={headCells} search={search}/>
        </TableContainer>
        <Button className={classes.button} color="primary" variant="contained" onClick={() => { history.push("/accueil-formations")}}> retour</Button>
        <Button className={classes.button} color="primary" variant="contained" onClick={() => { history.push("/sessions/complete")}}> Formations completes</Button>
        <Button className={classes.button} color="primary" variant="contained" onClick={() => { history.push("/sessions/terminee")}}> Formations terminées</Button>
    </Card>
    )
}
