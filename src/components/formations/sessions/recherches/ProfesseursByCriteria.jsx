import React, {useState, useEffect} from 'react'
import * as Yup from 'yup'


import ProfesseurService from '../../../../services/professeur/ProfesseurService'
import { Formik, Form } from 'formik';
import { Button } from '@material-ui/core';

import { Field} from 'formik';
import RadioGroup from '@material-ui/core/RadioGroup'
import  FormLabel  from '@material-ui/core/FormLabel';
import  FormControl  from '@material-ui/core/FormControl';
import  FormControlLabel  from '@material-ui/core/FormControlLabel';
import  Radio  from '@material-ui/core/Radio';
import { Fragment } from 'react';




export default function ProfesseursyCriteria(props) {

    const { idCategorie, dateDebut, dateFin} = props.criteriaProfesseur;
    const [professeurs, setProfesseurs] = useState([]);
    const editForProfesseur = props.editForProfesseur

    const [prof, setProf] = useState("")

    useEffect(  () => {
         ProfesseurService.findProfsByCriteria( idCategorie, dateDebut, dateFin)
                        .then(response => { setProfesseurs(response.data) })
    }, [ idCategorie, dateDebut, dateFin])


    const initialValues = {
        professeur: ""
    }
       
    const validationSchema = Yup.object({
        professeur : Yup.string().required("requis")

    })

    const handleSubmit = (values) => {
        if(values !== ""){
            let selectedProf = professeurs.filter((s) => s.nom.includes(values))
            editForProfesseur(selectedProf[0]);
        }else{
            props.setOpenPopupProfesseur(false)
        }

    }

    const handleClick = (e) => {
            setProf(e.target.value)
    }

    return (
        <>
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >

                
                {({ isSubmitting, isValid}) => {
                        return (
                            <>
                             <Form>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Veuillez choisir un professeur</FormLabel>
                                    <Field component={RadioGroup}  name="prof" value={prof} onChange={handleClick}>
                                        {
                                            professeurs.map(profFetch =>(
                                                <Fragment key={profFetch.idSalle}>
                                                <Field 
                                                    component={FormControlLabel}
                                                    checked={prof === profFetch.nom}
                                                    control={<Radio />}
                                                    value={profFetch.nom} 
                                                   // label={profFetch.nom}
                                                    name="prof"
                                                />
                                                <label htmlFor={profFetch.nom}> {profFetch.nom}  {profFetch.prenom}</label>
                                                {/* <input type="hidden" name="idSalle" value={salle.idSalle}/> */}
                                                </Fragment>
                                            ))
                                        }
                                    </Field>
                                    {/* <FormHelperText>Ce champs est requis.</FormHelperText> */}
                                    <Button disabled={isSubmitting || !isValid  }  variant="contained" color="primary" type="submit" onClick={() => handleSubmit(prof)} > Valider</Button>
                                </FormControl>


                              </Form>
                              
                            </>
                        )
                }}

            </Formik>
        </>
    )
}
