import React, {useState, useEffect} from 'react'
import * as Yup from 'yup'


import SalleService from '../../../../services/salle/SalleService'
import { Formik, Form } from 'formik';
import { Button } from '@material-ui/core';

import { Field} from 'formik';
import RadioGroup from '@material-ui/core/RadioGroup'
import  FormLabel  from '@material-ui/core/FormLabel';
import  FormControl  from '@material-ui/core/FormControl';
import  FormControlLabel  from '@material-ui/core/FormControlLabel';
import  Radio  from '@material-ui/core/Radio';

import { Fragment } from 'react';




export default function SallesByCriteria(props) {

    const { debutSession, finSession, accessibilite, capaciteMax} = props.criteriaSalle;
    const [salles, setSalles] = useState([]);
    const editForSalle = props.editForSalle

    const [salle, setSalle] = useState("")

    useEffect(  () => {
         SalleService.findSallesByCriteria(debutSession, finSession, accessibilite, capaciteMax)
                        .then(response => { setSalles(response.data) })
    }, [debutSession, finSession, accessibilite, capaciteMax ])


    const initialValues = {
        salle: "",
    }

    const validationSchema = Yup.object({
        salle : Yup.string().required("requis")

    })

    const handleSubmit = (values) => {

        if(values !== ""){
            let selectedSalle = salles.filter((s) => s.nom.includes(values))

            editForSalle(selectedSalle[0]);
        }else{

            props.setOpenPopupSalle(false)
        }

    }

    const handleClick = (e) => {
            setSalle(e.target.value)
    }

    return (
        <>
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >

                
                {({  isValid, values}) => {
                        return (
                            <>
                             <Form>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Veuillez choisir une salle</FormLabel>
                                    <Field component={RadioGroup}  name="salle" value={salle} onChange={handleClick}>
                                        {
                                            salles.map(salleFetch =>(
                                                <Fragment key={salleFetch.idSalle}>
                                                <Field 
                                                    component={FormControlLabel}
                                                    checked={salle === salleFetch.nom}
                                                    control={<Radio />}
                                                    value={salleFetch.nom} 
                                                    label={salleFetch.nom}
                                                    name="salle"
                                                />
                                                {/* <input type="hidden" name="idSalle" value={salle.idSalle}/> */}
                                                </Fragment>
                                            ))
                                        }
                                    </Field>
                                    {/* <FormHelperText>Ce champs est requis.</FormHelperText> */}
                                    <Button disabled={ !isValid  }  variant="contained" color="primary" type="submit" onClick={() => handleSubmit(salle)} > Valider</Button>
                                </FormControl>


                              </Form>
                              
                            </>
                        )
                }}

            </Formik>
        </>
    )
}
