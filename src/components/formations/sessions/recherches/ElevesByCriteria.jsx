import React, {useState, useEffect} from 'react'

import EleveService from '../../../../services/eleve/Eleve-service'
import { Formik, Form, Field } from 'formik';
import { FormControl, FormLabel, Button,  Table, TableHead, TableRow, TableCell, TableBody, } from '@material-ui/core';
import './checkbox.css';
function CheckboxForm(props) {
    return (
      <Field name={props.name}>
        {({ field, form }) => (
          <label>
            <input
              type="checkbox"
              {...props}
              checked={field.value.includes(props.value)}
              onChange={() => {
                if (field.value.includes(props.value)) {
                  const nextValue = field.value.filter(
                    value => value !== props.value
                  );
                  form.setFieldValue(props.name, nextValue);
                } else {
                  const nextValue = field.value.concat(props.value);
                  form.setFieldValue(props.name, nextValue);
                }
              }}
            />
          </label>
        )}
      </Field>
    );
  }


  const headCells = [
    "nom", "note", "choisir"
  ]

export default function ElevesByCriteria(props) {

    const { idModele, dateDebut, dateFin} = props.criteriaEleves;
    const [eleves, setEleves] = useState([]);
    const editForEleves = props.editForEleves



    useEffect(  () => {
        EleveService.findElevesByCriteria( idModele, dateDebut, dateFin)
                        .then(response => { setEleves(response.data) })
    }, [ idModele, dateDebut, dateFin])



    const handleSubmit = (values) => {

        editForEleves(values.checked);
    }

    const initialValues = {
        checked: []
    }



    return (
        <>
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
            >
                {({ isSubmitting, isValid}) => {
                    return (
                        <>
                            <Form>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Veuillez choisir des eleves</FormLabel>
                                    <Table> 
                                      <TableHead> 
                                        <TableRow>
                                          {headCells.map(head =>(
                                            <TableCell>{head}</TableCell>
                                          ))}  
                                        </TableRow>
                                      </TableHead> 
                                      <TableBody role="group" aria-labelledby="checkbox-group">
                                          {
                                              eleves.map(eleveFetch => (
                                                  <TableRow>
                                                      <TableCell>{eleveFetch.nom} {eleveFetch.prenom}</TableCell>
                                                      <TableCell>{eleveFetch.note}</TableCell>
                                                      <TableCell><CheckboxForm name="checked" value={eleveFetch} /></TableCell>
                                                  </TableRow>
                                              ))
                                          }
                                      </TableBody>
                                    </Table>
                                    
                                    <Button disabled={isSubmitting || !isValid }  variant="contained" color="primary" type="submit"  > Valider</Button>

                                </FormControl>
                            </Form>
                        </>
                )}}
               
            </Formik>
        </>
    )
}
