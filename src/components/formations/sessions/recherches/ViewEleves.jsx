import React, {useState} from 'react';


import { Table, TableHead, TableRow, TableCell, TableBody, TablePagination, IconButton } from '@material-ui/core';
import  CloseIcon  from '@material-ui/icons/Close';


const headCells = [
    "Nom", "Prenom", "Mail",  "Note", "Action"
]



export default function ViewEleves(props) {

    const [eleves, setEleves ] = useState(props.eleves);
    const deleteForEleve = props.deleteForEleve;
    const [search] = useState("")


    let pages = [ 5 , 10,  25];
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

    const elevesDisplaying = () => { return eleves.filter((s) => s.nom.toLowerCase().includes(search))}

    const handleChangePage = (event, newPage) => { setPage(newPage)}
    
    const handleChangeRowsPerPage = (e) => {
        setRowsPerPage(parseInt(e.target.value,10)); 
        setPage(0)}

    const elevesAfterPagingAndSorting = () => {return elevesDisplaying().slice( page * rowsPerPage, (page+1) * rowsPerPage)}


    const eleveFilter = (idEleve) => {
        return eleves.filter((e) =>  e.idEleve === idEleve);
    }

    const deleteEleve = async (idEleve) => {
        let newList = eleveFilter(idEleve);
        setEleves(newList);
        await deleteForEleve(idEleve);   
    }


    return (
        <>
            <Table>
                <TableHead>
                    <TableRow>
                      {
                        headCells.map(head => (
                            <TableCell> {head} </TableCell>   
                        ))
                       }
                    </TableRow>
                    </TableHead>
                    <TableBody>
                       { eleves != null || eleves.length > 0
                        ? elevesAfterPagingAndSorting().map(eleve => (
                            <TableRow key={eleve.idEleve}>
                                
                                <TableCell>{eleve.nom}</TableCell>
                                <TableCell>{eleve.prenom}</TableCell>
                                <TableCell>{eleve.email}</TableCell>
                                <TableCell>{eleve.note}</TableCell>
                                <TableCell>
                                    <IconButton onClick={() => {deleteEleve(eleve.idEleve)}} >
                                        <CloseIcon color="primary"/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))
                        : <TableRow> <p> Il n'y a pas encore d'élèves dans cette formation</p></TableRow>
                       }
                    </TableBody>

                <TablePagination
                           page = {page}
                           rowsPerPageOptions={pages}
                           rowsPerPage={rowsPerPage}
                           count={eleves.length}
                           onChangePage = {handleChangePage}
                           onChangeRowsPerPage = {handleChangeRowsPerPage }
                           color="primary"
                   />
            </Table>

        </>
    )
}
