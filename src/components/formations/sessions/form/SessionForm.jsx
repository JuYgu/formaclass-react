import React, {useEffect, useState} from 'react'

import * as Yup from 'yup'
import  {useHistory}  from 'react-router-dom';
import SessionService  from '../../../../services/session/Session-service';
import ModeleService  from '../../../../services/Modele/Modele-service';
import { Card, Container, TableContainer, Typography,  Button, makeStyles,  Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@material-ui/core';
import { Form, Formik, Field } from 'formik';
import FormikField from './../../../reusableComponent/formikField/Input';
import '../recherches/checkbox.css'
import classNames from "classnames";


  // Radio input
  const RadioButton = ({
    field: { name, value, onChange, onBlur },
    id,
    label,
    className,
    ...props
  }) => {
    return (
      <div>
        <input
          name={name}
          id={id}
          type="radio"
          value={id} // could be something else for output?
          checked={id === value}
          onChange={onChange}
          onBlur={onBlur}
          className={classNames("radio-button")}
          {...props}
        />
        <label htmlFor={id}>{label}</label>
      </div>
    );
  };

// Input feedback
const InputFeedback = ({ error }) =>
  error ? <div className={classNames("input-feedback")}>{error}</div> : null;

const RadioButtonGroup = ({
    value,
    error,
    touched,
    id,
    label,
    className,
    children
  }) => {
    const classes = classNames(
      "input-field",
      {
        "is-success": value || (!error && touched), // handle prefilled or user-filled
        "is-error": !!error && touched
      },
      className
    );
  
    return (
      <div className={classes}>

          {children}
          {touched && <InputFeedback error={error} />}

      </div>
    );
  };

  const useStyle = makeStyles({
      date : {
          '.MuiInputBase' : {
              input:{
                textAlign: "end"
              }
              
          }
      }
  })

function formatDate(date){
    return new Date(date).toLocaleDateString()
}

// const headCells = [
//     "Titre", "Description", "Choisir"
//   ]

export default function SessionForm() {

    const classes = useStyle();
    const history = useHistory()
    const [modeles, setModeles] = useState([]);

    useEffect(() => {
        ModeleService.getAllModels()
                        .then(response => {
                            setModeles(response.data);
                        });
    }, [])

    const initialValues = {
        modele: '',
        libelle: '',
        dateDebut:'',
        dateFin: ''
    }

    const sessionSchema = Yup.object().shape({
        modele : Yup.string().required('Champs requis'),
        libelle : Yup.string().min(3, 'Minimum 3 caracteres').required("Champs requis"),
        dateDebut : Yup.date().required('Champs requis'),
        dateFin : Yup.date().min(
            Yup.ref('dateDebut'), ({min}) => `Cette date doit être après ${formatDate(min)}` 
        ).required("Champs requis")
    })

    const handleSubmit = (values) => {

        SessionService.createSession( values.libelle, values.dateDebut, values.dateFin, values.modele)
                        .then( response => {
                            let session = response.data;
                            history.push({
                            pathname : "/sessions/session",
                            state: {session}});
                        })
                    
    }

    return (
        <Container>
              <Typography variant="h1"> Formation</Typography>
              <Card className="card">
                <Typography variant="h4"> Creation Session</Typography>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={handleSubmit}
                        validationSchema={sessionSchema}
                    >
                          {({ dirty, isValid, values, errors, touched, isSubmitting}) => {
                               return (

                                <Form>

                                        
                                        <TableContainer component={Paper} className="container" stickyHeader>
                                        <Table className="table">
                                            <TableHead>
                                                <TableRow>
                                                    
                                                        <TableCell>Choissiez un modele</TableCell>

                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                <RadioButtonGroup
                                                    id="modele"
                                                    
                                                    value={values.modele}
                                                    error={errors.modele}
                                                    touched={touched.modele}
                                                >
                                                    {
                                                        modeles.map((modele) => (
                                                            <TableRow key={modele.idModele}>
                                                                
                                                                <TableCell>
                                                                    <Field
                                                                        component={RadioButton}
                                                                        name="modele"
                                                                        id= {modele.idModele.toString()}
                                                                        />
                                                                </TableCell>
                                                                <TableCell> {modele.titre}</TableCell>
                                                            </TableRow>
                                                        ))
                                                    }
                                                </RadioButtonGroup>
                                            </TableBody>           
                                           
                                        </Table>
                                        </TableContainer>
                                        <FormikField label="Libelle" name="libelle"/>
                                        <FormikField className={classes.date} label="Date Debut" name="dateDebut" type="date"/>
                                        <FormikField  className={classes.date} label="Date Fin" name="dateFin" type="date" />


                                        <div className="button">
                                            <Button disabled={!dirty || !isValid } variant="contained" color="primary" type="submit" >
                                                Valider
                                            </Button>
                                            <Button variant="contained" color="secondary" onClick={ () => history.push("/accueil-formations")}>Retour</Button>
                                        </div>

                                </Form>
                               )
                          }}

                    </Formik>

              </Card>
            
        </Container>
    )
}
