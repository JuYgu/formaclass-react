import React, { useState } from 'react'

import { TableCell, IconButton } from '@material-ui/core';
import  EditOutinedIcon  from '@material-ui/icons/EditOutlined';
import CloseIcon  from '@material-ui/icons/Close';
import PopUp from './../../../reusableComponent/popUp/PopUp';
import UpdateModele from '../UpdateModele';

function DisplayModele(props)  {

        const [openPopup, setOpenPopup] = useState(false)
        const {idModele, titre, description, categorie, eleveMin, eleveMax} = props.modele;



        const openInPopup = ()  => {
            setOpenPopup(true)
        }



        return (
            <>
                <TableCell>{titre}</TableCell>
                <TableCell>{description}</TableCell>
                <TableCell>{categorie.type}</TableCell>
                <TableCell>{eleveMax}</TableCell>
                <TableCell>{eleveMin}</TableCell>
                <TableCell>
                    <IconButton   onClick= {() => {openInPopup()}}  >
                        <EditOutinedIcon color="primary"  />
                    </IconButton>
                    <IconButton onClick={() => {props.deleteModele(idModele)}} >
                        <CloseIcon color="primary"/>
                    </IconButton>
                </TableCell>

                <PopUp
                    title = {`Modification ${titre}`}
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                >
                    <UpdateModele modele = {props.modele} categories = {props.categories} editModele={props.editModele} openPopup={openPopup} setOpenPopup={setOpenPopup} />

                </PopUp>
                
            </>
        )
    
}
export default DisplayModele
