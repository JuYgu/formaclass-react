import React, { Component } from 'react'
import ModeleService from '../../../../services/Modele/Modele-service'
import CategoryService from '../../../../services/category/Category-service';
import { Card, Typography, Paper, TableContainer, Toolbar, Button } from '@material-ui/core';


import SearchBar from './../../../reusableComponent/seachBar/searchBar';
import TableModele from './TableModele';
import  AddIcon  from '@material-ui/icons/Add';


const headCells = [
    "Titre", "Description", "Categorie", "Eleves Max", "Eleves Min", "Action"
]

let ok
export default class ListModele extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             modeles: [],
             categories : [],
             isLoaded: false,
             isLoadedCategorie: false,
             searchvalue : '',
             openPopup : false
        }
    }
    

    async componentDidMount(){
       await this.getModeles();
       await this.getAllCategories();
       if(this.state.isLoaded === true && this.state.isLoadedCategorie){ ok = this.state.isLoaded;}
        console.log("ok", ok)
    }

    getModeles = async () => {
        const response = await ModeleService.getAllModels()
        await this.setState({ modeles: response.data })
        this.setState({isLoaded: true})
        console.log("modele", this.state.modeles)
    }

    getAllCategories = async () => {
        const response = await CategoryService.getAllCategory()
        await this.setState({ categories : response.data});
        this.setState({ isLoadedCategorie : true});
    }

    editModele = async (modele, idModele) => {
        // this.setState({
        //     isLoaded: false
        // })
        const response = await ModeleService.updateModel(idModele,modele.titre, modele.eleveMin, modele.eleveMax, modele.description, modele.categorie)
        await this.setState({ modeles : response.data })
        console.log(this.state.modeles)
        // this.setState({
        //     isLoaded: true
        // })


    }



    



    deleteModele = async (idModele) => {
       const response = await ModeleService.deleteModel(idModele)
                            .then((response) => {
                               let retour =  response.data.deleted ? true : false;
                               return retour;
                            })
        if(response){
            await this.setState({
                modeles : this.state.modeles.filter(modele => modele.idModele !== idModele)
            });
        }else{
            console.log("erreur")
        }
    }


    componentDidUpdate(prevProps, prevState){
        if(prevState.isLoaded !== this.state.isLoaded){
            
            ok = this.state.isLoaded
            console.log("c'est pas ok", ok)
        }
    }


    //Fonction de recherche 
    handleSearch = (e) => {
        console.log(e.target)
            this.setState({
                searchvalue: e.target.value
            });
            console.log("ok", ok)
    }

    render() {

        let rendu
        if (ok === false){
            rendu = <div>Loading</div>
        }else{
            rendu = <TableModele deleteModele={this.deleteModele} search={this.state.searchvalue} headCells={headCells} modeles={this.state.modeles} categories={this.state.categories} editModele={this.editModele}/>
        }

        return (
            <Card>
            <Card>
                <Typography variant="h3"> Modeles </Typography>
                <Toolbar>
                   <SearchBar handleSearch={this.handleSearch} />
                    <Button variant="contained" startIcon = {<AddIcon/>} onClick={() => this.props.history.push("/modeles/new")}>Ajouter Modèle</Button>
                </Toolbar>
                <TableContainer component={Paper}>
                   {rendu}
                </TableContainer>
            </Card>
            <Button onClick={() => { this.props.history.push("/accueil-formations")}}> retour</Button>
            </Card>
        )
    }
}
