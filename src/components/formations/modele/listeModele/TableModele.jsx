import React, {  useState } from 'react'
import { Table, TableHead, TableRow, TableCell, TableBody, TablePagination} from '@material-ui/core';
import DisplayModele from './DisplayModele';





export default function TableModele (props) {

        const headCells = props.headCells;
        const modeles = props.modeles
        const search = props.search;

        const modeleDisplaying = () => { return modeles.filter((m) => m.titre.toLowerCase().includes(search));}

        let pages = [ 5 , 10,  25];
        const [page, setPage] = useState(0);
        const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

        const handleChangePage = (event, newPage) =>{
            setPage(newPage)
        }
        const handleChangeRowsPerPage = event => {
            setRowsPerPage(parseInt(event.target.value,10));
            setPage(0) 
        }

        const modelesAfterPagingAndSorting= () => { return modeleDisplaying().slice( page * rowsPerPage , (page+1) * rowsPerPage) }
       


        return (
            <>
                 <Table>
                        <TableHead>
                            <TableRow>
                                {
                                    headCells.map(head => (
                                        <TableCell> {head} </TableCell>   
                                    ))
                                }
                            </TableRow>
                        </TableHead>
                            <TableBody>
                            {
                                modelesAfterPagingAndSorting().map( modele => (
                                    <TableRow key={modele.idModele}>

                                        <DisplayModele modele={modele} deleteModele={props.deleteModele} modeles={props.modeles} categories={props.categories} editModele={props.editModele}/>


                                    </TableRow>
                                ))
                            }
                            </TableBody>
                            <TablePagination
                                    page = {page}
                                    rowsPerPageOptions={pages}
                                    rowsPerPage={rowsPerPage}
                                    count={modeles.length}
                                    onChangePage = {handleChangePage}
                                    onChangeRowsPerPage = {handleChangeRowsPerPage }
                                    color="primary"
                            />
                    </Table>  
            </>
        )
    
}
