import React, { Component } from 'react'
import { Container, Card, Typography, Button } from '@material-ui/core';
import ModeleService from '../../../services/Modele/Modele-service';


export default class Modele extends Component {
    constructor(props) {
        super(props)

        this.state = {
             idModele : this.props.location.state.idModele,
             modele: "",
             categorie:""
        }
    }

    componentDidMount(){
        this.getModele();
    }
    
    getModele = () => {
        ModeleService.getModel(this.state.idModele)
                        .then(response => {
                            this.setState({
                                idModele : response.data.idModele,
                                modele: response.data,
                                categorie: response.data.categorie
                            })
                            console.log("la réponse bordel",response.data.categorie.type)
                        })
    }

    updateModel = () =>{

        this.props.history.push({
            pathname: "/modeles/update",
            state : {
                idModele: this.state.idModele,
                titre :  this.state.modele.titre,
                eleveMin:  this.state.modele.eleveMin,
                eleveMax:  this.state.modele.eleveMax,
                description :  this.state.modele.description,
                categorie :   this.state.categorie.idCategorie

            
            }
        })
    }

    render() {
        return (
            <Container>
                <Card>
                    <Typography variant="h4"> Modèle Formation</Typography>
                    <Card color="secondary">
                         <Typography variant="h6"> {this.state.modele.titre}</Typography>
                        <p> categorie </p> <Typography variant="h6"> {this.state.categorie.type}</Typography>
                        <p> Nombre de participants min </p> <Typography variant="h6"> {this.state.modele.eleveMin}</Typography>
                        <p> Nombre de participants max </p> <Typography variant="h6"> {this.state.modele.eleveMax}</Typography>
                        <p> Description </p> <Typography variant="h6"> {this.state.modele.description}</Typography>
                    </Card>
                    <Button variant="contained" color="primary" onClick={this.updateModel}>Modifier</Button>
                    <Button variant="contained" color="secondary" onClick={this.props.history.goBack()}>Retour</Button>
                </Card>                
            </Container>
        )
    }
}
