import React from 'react'

import * as Yup from 'yup'
import { Form, Formik } from 'formik';
import FormikField from '../../reusableComponent/formikField/Input';
import FormikSelect from '../../reusableComponent/formikSelect/Select';
import Button  from '@material-ui/core/Button';






export default function UpdateForm (props) {

    const initialValues = {
        titre :  props.modele.titre,
        eleveMin:  props.modele.eleveMin,
        eleveMax:  props.modele.eleveMax,
        description : props.modele.description,
        categorie :  props.modele.categorie
     }

     const idModele = props.modele.idModele

    // constructor(props) {
    //     super(props)
    
    //     this.state = {
    //          categories : "",
    //          initialValues : {
    //             titre :  this.props.modele.titre,
    //             eleveMin:  this.props.modele.eleveMin,
    //             eleveMax:  this.props.modele.eleveMax,
    //             description :  this.props.modele.description,
    //             categorie :   this.props.modele.categorie
    //          },
    //          idModele : this.props.modele.idModele,

    //          modele : "",
    //          isLoadedModele : false,
    //     }
        
    // }
    const modelSchema = Yup.object().shape({
        titre : Yup.string()
                .min(2, "Minimum 2 caractères")
                .required('Champs requis'),
        eleveMin : Yup.number()
                .required('Champs requis'),
        eleveMax : Yup.number()
                .required('Champs requis'),
        description : Yup.string()
                .min(2, "Minimum 2 caractères")
                .required('Champs requis'),
        categorie : Yup.string()
                .required('Champs requis'),
    })



    // componentDidMount(){
    //     this.getAllCategories();
    // }



    // getAllCategories = () => {
    //     CategoryService.getAllCategory()
    //     .then( response => {
    //         console.log(response.data);
    //             this.setState({ categories : response.data,  isLoadedCategorie : true});
    //         });
    // }
    

    const handleSubmit = async (values) => {
        console.log("values", values)
       await props.editModele(values,idModele)
        props.setOpenPopup(false)
        
                    
    }



        return (
                <>


                       
                        <Formik 
                        initialValues={initialValues}
                        onSubmit={handleSubmit}
                        validationSchema={modelSchema}
                             >
                        {({  isValid, isSubmitting}) => {
                            return (
                                <Form>
                                        <FormikField label="Titre" name="titre"/>
                                        <FormikSelect name="categorie" label="Catégorie" defaultValue={initialValues.categorie.id}  categories={props.categories} value="type"/>
                                        <FormikField label="Nombre d'élèves minimum" name="eleveMin" type="number"  />
                                        <FormikField label="Nombre d'élèves maximum" name="eleveMax" type="number" />
                                        <FormikField label="Description" name="description"  />

                                        <div className="button">
                                            <Button disabled={ !isValid || isSubmitting } variant="contained" color="primary" type="submit" >
                                                Valider
                                            </Button>
                                        </div>

                                </Form>
                            )
                        }}
                     </Formik>
                    
                   
                </>

        )
    
}
