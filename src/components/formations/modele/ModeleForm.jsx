import React, { Component } from 'react'

import * as Yup from 'yup'
import { Container, Typography, Card } from '@material-ui/core';
import { Form, Formik } from 'formik';
import FormikField from './../../reusableComponent/formikField/Input';
import FormikSelect from './../../reusableComponent/formikSelect/Select';
import CategoryService from '../../../services/category/Category-service';
import Button  from '@material-ui/core/Button';
import ModeleService from '../../../services/Modele/Modele-service';


const initialValues = {
    titre : "",
    eleveMin: "",
    eleveMax: "",
    description : "",
    categorie : "" 
}

const modelSchema = Yup.object().shape({
    titre : Yup.string()
            .min(2, "Minimum 2 caractères")
            .required('Champs requis'),
    eleveMin : Yup.number()
            .required('Champs requis'),
    eleveMax : Yup.number()
            .required('Champs requis'),
    description : Yup.string()
            .min(2, "Minimum 2 caractères")
            .required('Champs requis'),
    categorie : Yup.string()
            .required('Champs requis'),
})

export default class ModeleForm extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             categories : "",
             isLoaded : false
        }
        
    }

    componentDidMount(){
        this.getAllCategories();
    }

    getAllCategories = () => {
        CategoryService.getAllCategory()
        .then( response => {

                this.setState({ categories : response.data, isLoaded: true});
            });
    }
    

    handleSubmit = (values) => {
        ModeleService.createModel( values.titre, values.eleveMin, values.eleveMax, values.description, values.categorie)
                        .then( response => {
                            this.props.history.push("/modeles");
                        })
                    
    }

    render() {

        return (
            <Container>
                <Typography variant="h1"> Formation</Typography>

                <Card>
                    <Typography variant="h4"> Creation Modèle </Typography>
                    {this.state.isLoaded &&
                        <Formik 
                        initialValues={initialValues}
                        onSubmit={this.handleSubmit}
                        validationSchema={modelSchema}
                             >
                        {({ dirty, isValid}) => {
                            return (
                                <Form>
                                        <FormikField label="Titre" name="titre"  />
                                        <FormikSelect name="categorie" label="Catégorie"  categories={this.state.categories} value="type"/>
                                        <FormikField label="Nombre d'élèves minimum" name="eleveMin" type="number" />
                                        <FormikField label="Nombre d'élèves maximum" name="eleveMax" type="number" />
                                        <FormikField label="Description" name="description"  />

                                        <div className="button">
                                            <Button disabled={!dirty || !isValid } variant="contained" color="primary" type="submit" >
                                                Valider
                                            </Button>
                                            <Button variant="contained" color="secondary" onClick={() => {this.props.history.goBack()}}>Retour</Button>
                                        </div>

                                </Form>
                            )
                        }}
                     </Formik>
                    }
                   
                </Card>
            </Container>
        )
    }
}
