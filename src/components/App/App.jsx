import React from 'react';

import "./App.css";

import Routes from './../routes/Routes';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core';
import Theme from '../../Theme.jsx'

function App() {
    return (
        <>
            <MuiThemeProvider theme={Theme}>
                <BrowserRouter>
                    <Routes/>
                </BrowserRouter>
            </MuiThemeProvider>

        </>
    );
}
export default App;