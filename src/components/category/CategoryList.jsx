import React, { useState, useEffect } from 'react'
import { Card, makeStyles, TextField, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TablePagination, TableSortLabel, Typography, Toolbar, Button } from '@material-ui/core';
import {useHistory} from 'react-router-dom';
import EditOutinedIcon from '@material-ui/icons/EditOutlined';
import  CloseIcon  from '@material-ui/icons/Close';
import CategoryService from '../../services/category/Category-service'
import '../../css/category.css';
import PopUp from './../reusableComponent/popUp/PopUp';
import CategoryForm from './CategoryForm';
import ActionButton from '../reusableComponent/button/ActionButton';





const useStyles = makeStyles(theme => ({
    table: {
        '& thead th': {
            fontWeight: '600',
            color : theme.palette.primary.light,
            backgroundColor: theme.palette.primary
        },
        '& tbody tr:hover': {
            backgroundColor: "#ecd98e",
            cursor: 'pointer'
        }
    },
    button:{

    }
}))



export default function CategoryList()  {

    const classes = useStyles();
    const history = useHistory();
    const pages = [2 , 10 , 25];
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

    const [categories , setCategories] = useState([]);
    const [categorieForEdit, setCategorieForEdit] = useState(null);


    const [order, setOrder] = useState();
    const [orderBy, setOrderBy] = useState();

    const [search, setSearch] = useState('');


    const [openPopup, setOpenPopup] = useState(false)



    const addOrEdit = (category) => {

        console.log("Dans le click");
        if(category.idCategorie == null){
            CategoryService.createCategory(category)
            .then( (response) => {
                console.log("C'est créé", response)
                setCategories(response.data)
            })
            
        }else{
            console.log(category.idCategorie, category.type)
            CategoryService.updateCategory(category.idCategorie, category.type)
                            .then( (response) => {
                                console.log("C'est update", response)
                                setCategories(response.data)
                            }, error =>{
                                console.log("c'est pas update")
                            })        
        }


        setOpenPopup(false);
       
        
    }

    const openInPopup = item  => {
        console.log('item',item)
        setCategorieForEdit(item);
        setOpenPopup(true)
    }

    // changer tous les 1 par cellId
    const handleSortRequest = () => {
        const isAsc = orderBy === 1 && order === "asc";
        setOrder(isAsc ? 'desc' : 'asc')
        setOrderBy(1)
    }

    const handleChangePage = (event, newPage) =>{
        setPage(newPage);
    }
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value,10));
        setPage(0);
    }

    const handleSearch = e => {
      setSearch(e.target.value)
    }

    const catDisplaying = () => {return categories.filter((c) => c.type.toLowerCase().includes(search))}



    // FONCTIONS DE COMPARAISON POUR LE TRI PAR TABLEHEAD
    // function stableSort(array, comparator){
    //     const stabilizedThis = array.map((el, index) => [el, index]);
    //     stabilizedThis.sort((a, b) => {
    //         const order = comparator(a[0], b[0]);
    //         if(order !== 0) return order;
    //         return a[1] - b[1];
    //     });
    //     return stabilizedThis.map((el) => el[0]);
    // }

    // function getComparator(order, orderBy){
    //     return order === 'desc'
    //         ? (a, b) => descendingComparator(a, b, orderBy)
    //         : (a , b) => -descendingComparator(a, b, orderBy);
    // }

    // function descendingComparator( a, b, orderBy){
    //     if (b[orderBy] < a[orderBy]){
    //         return -1;
    //     }
    //     if(b[orderBy] > a[orderBy]){
    //         return 1;
    //     }
    //     return 0
    // }

    const categoriesAfterPagingAndSorting = () => {
        return catDisplaying().slice( page * rowsPerPage , (page+1) * rowsPerPage)
    }

    useEffect( () => {  
        CategoryService.getAllCategory()
                            .then((res) => { 
                            setCategories(res.data);
        })
    }, []);


    const deleteCategorie = async (idCategorie) => {

        CategoryService.deleteCategory(idCategorie)
                        .then(res => {
                            setCategories(res.data)
                        }, error =>{
                            console.log("VOus ne pouvez pas supprimer cette categorie")
                        })
    }

 

        return (
            <>
            
                <Card>
                    
                    <Typography variant="h3"> Catégories </Typography>
                        <Toolbar>
                            <form className="form">
                                <TextField
                                    label="rechercher"
                                    onChange = {handleSearch}
                                />
                            </form>
                           
                        </Toolbar>

                        <TableContainer component={Paper}>

                            <Table className={classes.table}>

                                <TableHead>
                                    <TableRow>
                                        <TableSortLabel 
                                            active = {orderBy === 1}
                                            direction = {orderBy === 1 ? order:'asc'}
                                            onClick={ () => {handleSortRequest()}}
                                        >
                                            <TableCell> Type </TableCell>
                                        </TableSortLabel>
                                        <TableCell> Action </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                {
                                    categoriesAfterPagingAndSorting().map( categorie => 
                                        (
                                        <TableRow key={categorie.idCategorie}>
                                            <TableCell>{categorie.type}</TableCell>
                                            <TableCell>
                                                <ActionButton 
                                                    color="primary"
                                                    onClick= {() => {openInPopup(categorie)}}
                                                >
                                                    <EditOutinedIcon />
                                                </ActionButton>
                                                <ActionButton 
                                                    color="secondary"
                                                    onClick= {() => deleteCategorie(categorie.idCategorie)}
                                                >
                                                    <CloseIcon/>
                                                </ActionButton>
                                            </TableCell>
                                        </TableRow>
                                        ))
                                }
                                </TableBody>
                                <TablePagination
                                    page = {page}
                                    rowsPerPageOptions={pages}
                                    rowsPerPage={rowsPerPage}
                                    count={categories.length}
                                    onChangePage = {handleChangePage}
                                    onChangeRowsPerPage = { handleChangeRowsPerPage }
                                    color="primary"
                                />
                            </Table>
                          
                        </TableContainer>
                        <Button className={classes.button} onClick={() => { history.push("/accueil-formations")}} variant="contained"> retour</Button>

                </Card>
                <PopUp
                    title = "Créer une categorie"
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                >
                    <CategoryForm addOrEdit={addOrEdit} categorieForEdit={categorieForEdit}/>

                </PopUp>

            </>
            
        )
    
}
