import React, { useState } from 'react'

import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { Button, Typography, Card, Container } from '@material-ui/core';
import FormikField from '../reusableComponent/formikField/Input';
import CategoryService from '../../services/category/Category-service';
import { useHistory } from 'react-router-dom';



export default function CategoryForm(props) {



    const history = useHistory();
    const [initialValues] = useState({ type: "" });


    const categorieSchema = Yup.object().shape({
        type: Yup.string()
            .min(1, 'Minimum 1 caractere')
            .required('Veuillez renseigner ce champs'),
    })

    const handleSubmit = values => {
        CategoryService.createCategory(values)
            .then((response) => {
                history.push("/categories")

            })
    }

    return (
        <>
            <Container>
                <Typography variant="h1"> Formation</Typography>

                <Card>
                    <Typography variant="h4"> Creation Categorie </Typography>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={handleSubmit}
                        validationSchema={categorieSchema}
                    >

                        {({ dirty, isValid, isSubmitting }) => {
                            return (
                                <>

                                    <Form>
                                        <FormikField label="Categorie" value={initialValues.type} name="type" />
                                        <Button disabled={!dirty || !isValid || isSubmitting} variant="contained" color="primary" type="submit" > Valider</Button>
                                    </Form>
                                </>
                            )
                        }}

                    </Formik>
                </Card>
            </Container>

        </>
    )



}
