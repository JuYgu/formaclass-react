import React, {useEffect, useState} from 'react'

import  * as Yup  from 'yup';
import { Formik, Form } from 'formik';
import { Button } from '@material-ui/core';
import FormikField from './../reusableComponent/formikField/Input';



export default function CategoryForm(props) {


    const {addOrEdit, categorieForEdit} = props

    const [initialValues , setInitialValues] = useState({type : categorieForEdit.type, idCategorie: categorieForEdit.idCategorie});

 
    useEffect(() => {
        setInitialValues({type : categorieForEdit.type, idCategorie: categorieForEdit.idCategorie })
    }, [categorieForEdit] )
    
    const categorieSchema = Yup.object().shape({
        type: Yup.string()
                    .min(1, 'Minimum 1 caractere')
                    .required('Veuillez renseigner ce champs'),
        idCategorie : Yup.string()
    })

    const handleSubmit = values => {
        addOrEdit(values)
    }

        return (
            <>
                <Formik
                    initialValues={initialValues}
                    onSubmit = {handleSubmit}
                    validationSchema = {categorieSchema}
                >
    
                    {({ dirty, isValid, isSubmitting}) => {
                        return (
                            <>
                              
                              <Form>
                                  <FormikField label="Categorie" value={initialValues.type}  name="type" />
                                  <Button disabled={!dirty || !isValid || isSubmitting }  variant="contained" color="primary" type="submit" > Valider</Button>
                              </Form>
                            </>
                        )
                    }}
    
                </Formik>

            </>
        )

        

}
