import React, { Component } from 'react'
import {Switch, Route} from 'react-router-dom';
import Inscription from '../auth/inscription/Inscription';
import MailForgetPassword from '../auth/forget-password/MailForgetPassword';
import Login from './../auth/login/Login';
import PassForgetPassword from '../auth/forget-password/PassForgetPassword';
import ConfirmationInscription from './../auth/inscription/ConfirmationInscription';
import CategoryList from './../category/CategoryList';
import CategoryCreate from './../category/CategoryCreate';
import AccueilSalle from '../salle/AccueilSalle';
import ListeSalle from '../salle/ListeSalle';
import Salle from '../salle/Salle';
import UpdateSalle from '../salle/UpdateSalle';
import NouvelleSalle from '../salle/NouvelleSalle';
import AccueilProfesseur from '../professeur/AccueilProfesseur';
import ListeProfesseur from '../professeur/ListeProfesseur';
import Professeur from '../professeur/Professeur';
import UpdateProfesseur from '../professeur/UpdateProfesseur';
import NouveauProfesseur from '../professeur/NouveauProfesseur';
import AccueilServiceFormation from './../formations/AccueilServiceFormation';
import ModeleForm from './../formations/modele/ModeleForm';
import Modele from './../formations/modele/Modele';
import UpdateModele from './../formations/modele/UpdateModele';
import ListModele from './../formations/modele/listeModele/ListModele';
import SessionProfesseur from '../professeur/SessionProfesseur';
import ListSessionProfesseur from '../professeur/ListSessionProfesseur';
import AccueilEquipement from '../equipement/AccueilEquipement';
import NouvelEquipement from '../equipement/NouvelEquipement';
import AccueilEleve from '../eleve/AccueilEleve';
import ListeEleve from '../eleve/ListeEleve';
import Eleve from '../eleve/Eleve';
import UpdateEleve from '../eleve/UpdateEleve';
import NouvelEleve from '../eleve/NouvelEleve';
import SessionEleve from '../eleve/SessionEleve';
import ListSessionReal from '../eleve/ListeSessionReal';
import ImportCsv from '../eleve/ImportCsv';
import SessionsIncomplete from './../formations/sessions/block-sessions/SessionsIncomplete';
import SessionsComplete from '../formations/sessions/block-sessions/SessionsComplete';
import SessionView from './../formations/sessions/block-sessions/SessionView';
import SessionForm from './../formations/sessions/form/SessionForm';




export default class Routes extends Component {
    render() {
        return (
            <Switch>
                {/* AUTHENTIFICATION */}
                <Route exact path={["/", "/login"]} component={Login}/>
                <Route exact path="/inscription" component={Inscription}/>
                <Route exact path="/confirmation-inscription/:token" component={ConfirmationInscription}/>
                <Route exact path="/forget-password" component={MailForgetPassword}/>
                <Route exact path="/forget-password/:token" component={PassForgetPassword}/>
                
                {/* SALLES */}
                <Route exact path="/accueil-salle" component={AccueilSalle}/>
                <Route exact path="/liste-salles" component={ListeSalle}/>
                <Route exact path="/salle/:id" component={Salle}/>
                <Route exact path="/ajouter-salle" component={NouvelleSalle}/>
                <Route exact path="/editer-salle/:id" component={UpdateSalle}/>

                {/* PROFESSEURS */}
                <Route exact path="/accueil-professeur" component={AccueilProfesseur}/>
                <Route exact path="/liste-professeurs" component={ListeProfesseur}/>
                <Route exact path="/professeur/:id" component={Professeur}/>
                <Route exact path="/ajouter-professeur" component={NouveauProfesseur}/>
                <Route exact path="/editer-professeur/:id" component={UpdateProfesseur}/>    
                <Route exact path="/professeur-session/:id" component={SessionProfesseur}/> 
                <Route exact path="/professeur-listsessions/:id" component={ListSessionProfesseur}/>     

                  {/* ELEVES */}
                <Route exact path="/accueil-eleve" component={AccueilEleve}/>
                <Route exact path="/liste-eleves" component={ListeEleve}/>
                <Route exact path="/eleve/:id" component={Eleve}/>
                <Route exact path="/ajouter-eleve" component={NouvelEleve}/>
                <Route exact path="/editer-eleve/:id" component={UpdateEleve}/>    
                <Route exact path="/eleve-session/:id" component={SessionEleve}/> 
                <Route exact path="/eleve-realisees/:id" component={ListSessionReal}/>    
                <Route exact path="/eleve-upload" component={ImportCsv}/>  
    

                {/* EQUIPEMENTS */}
                <Route exact path="/accueil-equipement" component={AccueilEquipement}/>
                <Route exact path="/ajouter-equipement" component={NouvelEquipement}/>

                {/* SERVICE FORMATION */}
                <Route exact path="/accueil-formations" component={AccueilServiceFormation}/>
                    {/* SERVICE SESSION */}
                    <Route exact path="/sessions/new" component={SessionForm}/>
                    <Route exact path="/sessions/incomplete" component={SessionsIncomplete}/>
                    <Route exact path="/sessions/complete" component={SessionsComplete}/>
                    <Route exact path="/sessions/session" component={SessionView}/>
                    {/* SERVICE MODELE */}
                    <Route exact path="/modeles/new" component={ModeleForm}/>
                    <Route exact path="/modeles" component={ListModele}/>
                    <Route exact path="/modeles/current" component={Modele}/>
                    <Route exact path="/modeles/update" component={UpdateModele}/>
                    {/* SERVICE CATEGORIE */}
                    <Route exact path="/categories" component={CategoryList}/>
                    <Route exact path="/categories/new" component={CategoryCreate}/>

            </Switch>
        )
    }
}
