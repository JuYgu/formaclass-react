import React, { Component } from 'react'
import AuthService from '../../../services/auth/Auth-service';
import  * as Yup from 'yup';
import  Snackbar  from '@material-ui/core/Snackbar';
import MuiAlert  from '@material-ui/lab/Alert';
import { Formik, Form } from 'formik';
import FormikField from './../../reusableComponent/formikField/Input';
import { Button, Card, Container, Typography } from '@material-ui/core';
import { BeatLoader } from 'react-spinners';
import '../../../css/login.css';

function Alert(props){
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class PassForgetPassword extends Component {
    constructor(props){
        super(props);


        this.state = {
            token: this.props.match.params.token,
            snackbarOpen: false,
            snackbarMsg : "",
            severity: "",
            requeteOk : false,
            isLoaded: false
        }
    }

  

    componentDidMount(){

        AuthService.forgetPasswordCheckToken(this.state.token)
        .then(response => {
            this.setState({requeteOk: true, isLoaded : true})
            
        }, error => {
            this.setState({requeteOk: false, isLoaded : true})
        })
    }

    snackBarClose = (event) => {
        this.setState({snackbarOpen: false});
    }

    initialValues = {
        password:""
    }

    passwordSchema = Yup.object().shape({
        email: Yup.string().min(7, "Minimum 7 caractères").max(20, "Maximum 20 caractères").required("Veuillez indiquer votre nouveau password")
    })

 

    handleSubmit = (values) => {

        AuthService.forgetPasswordNewPass(this.state.token, values)
                    .then(
                        () => {
                            this.setState({snackbarOpen: true, snackbarMsg: "Votre mot de passe a bien été modifié", severity:"success"});
                            this.props.history.push("/login");
                        }, error => {
                            if(error){
                                this.setState({snackbarOpen: true, snackbarMsg: "Votre requête n'a pas abouti", severity:"error"});
                            }
                        }
                    );
        
            
    }
    

    render() {
        const isTokenValid = this.state.requeteOk
        let rendu;
        
        if(!this.state.isLoaded){
             rendu = <div className="spinner"> <BeatLoader  loading color=' #0a7568 ' size={72} /> </div>
        } else{
            if(isTokenValid){
                rendu =  
                <>
                <p> Veuillez indiquer votre nouveau mot de passe</p>
                <Formik
                initialValues={this.initialValues}
                onSubmit={this.handleSubmit}
                validationSchema={this.PasswordSchema}
            >
                {({ dirty, isValid, isSubmitting }) => {
                    return(
                        <Form>
                            <FormikField label="Password" name="password"  type="password" />
                            <Button disabled={!dirty || !isValid || isSubmitting} variant="contained" color="primary" type="submit">Valider</Button>
                        </Form>
                    );
                }}

            </Formik>
            </>
            }else{
                rendu = 
                <div>

                    <Typography variant="subtitle1" color="error" className="error">Le lien a expiré, veuillez recommencer.</Typography>
                </div>
            }
        }
        return (
            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
                <Card className="login">
                    <Typography variant="h3">Mot de passe oublié</Typography>
                    

                    <Snackbar
                        anchorOrigin={{vertical:'bottom', horizontal:'center'}}
                        open = {this.state.snackbarOpen}
                        autoHideDuration= {2000}
                        onClose={this.snackBarClose}
                    >
                        <Alert severity={this.state.severity} onClose={this.snackBarClose}>{this.state.snackbarMsg}</Alert>
                    </Snackbar>

                    {rendu}
                </Card>
            </Container>
        )
    }

}
