import React, { Component } from 'react'

import '../../../css/login.css';
import  * as Yup  from 'yup';
import AuthService from '../../../services/auth/Auth-service';
import { Formik, Form } from 'formik';
import FormikField from './../../reusableComponent/formikField/Input';
import  Button  from '@material-ui/core/Button';
import  Snackbar  from '@material-ui/core/Snackbar';
import  MuiAlert  from '@material-ui/lab/Alert';

import MailEnvoye from './MailEnvoye';
import { Card, Container, Typography } from '@material-ui/core';




const initialValues = {
    email: ""
}

const emailSchema = Yup.object().shape({
    email: Yup.string()
                .email('Email invalide')
                .required('Veuillez indiquer votre adresse mail')
})

function Alert(props){
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class MailForgetPassword extends Component {

    constructor(props){
        super(props);


        this.state = {
            snackbarOpen: false,
            snackbarMsg : "",
            severity: "",
            requeteOk : false
        }
    }

    snackBarClose = (event) => {
        this.setState({snackbarOpen:false});
    }



    handleSubmit = (values) => {
        AuthService.forgetPasswordPostMail(values.email)
        .then(
            () => {

                    this.setState({snackbarOpen: true, snackbarMsg:"Mail envoyé !", severity:"success"});

                
                this.setState({requeteOk : true});
            }, error => {
                if(error){
                    this.setState({snackbarOpen: true, snackbarMsg:"Ce mail est inconnu, veuillez recommencer", severity:"error"});
                }
                
            }
        )
    }

    render() {

        let rendu;
        if(this.state.requeteOk){
            rendu = <MailEnvoye/>
        }else{
            rendu = <Formik
            initialValues={initialValues}
            onSubmit={this.handleSubmit}
            validationSchema={emailSchema}
        >

            {({ dirty, isValid, isSubmitting }) => {
                return (
                    <>
                        <Typography variant="subtitle1"  > Veuillez indiquer votre adresse mail</Typography>
                        <Form>
                            <FormikField label="Adresse mail" name="email" type="email"/>
                            <div className="button">
                                <Button disabled={!dirty || !isValid || isSubmitting }  variant="contained" color="primary" type="submit" > Envoyer</Button>
                                <Button   variant="contained" color="secondary"  onClick={() => {this.props.history.push("/")}} > retour</Button>
                            </div>
                        </Form>
                    </>
                )
            }}


        </Formik>
        }

        return (
            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
            
                <Card className="login">

                    <Typography variant="h4" >Mot de passe oublié</Typography>
                    

                    <Snackbar
                        anchorOrigin={{vertical:'bottom', horizontal:'center'}}
                        open = {this.state.snackbarOpen}
                        autoHideDuration = {6000}
                        onClose={this.snackBarClose}
                    >
                            <Alert severity={this.state.severity}  onClose={this.snackBarClose}>{this.state.snackbarMsg}</Alert>
                    </Snackbar>

                    {rendu}
                    
                </Card>
            </Container>
        )
    }
}
