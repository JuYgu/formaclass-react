import { Button, Typography } from '@material-ui/core'
import React, { Component } from 'react'


export default class MailEnvoye extends Component {

    accueil = () =>{
        this.props.history.push('/login');
    }

    render() {
        return (
            <>
                <Typography variant="subtitle1">Un email vous a été envoyé sur votre adresse mail.</Typography>
                <Typography variant="subtitle1">Merci de cliquer sur le lien afin de pouvoir réinitialiser votre password</Typography>
                
                <Button color="primary" onClick={() => {this.accueil()}}>Retour a l'accueil</Button>
            </>
        )
    }
}
