import { Button, Card, CardContent, Typography } from '@material-ui/core'
import React, { Component } from 'react'

export default class InscriptionOk extends Component {

    accueil = () =>{
        this.props.history.push('/login');
    }

    render() {
        return (
            <Card>
                <CardContent>
                    <Typography variant="h1">Inscription</Typography>
                    <Typography variant="subtitle1">Votre inscription a bien été prise en compte.</Typography>
                    <Typography variant="subtitle1">Un email vous a été envoyé sur votre adresse mail.  Merci de cliquer sur le lien afin de confirmer votre inscription </Typography>
                    
                    <Button  color="primary" onClick={() => {this.accueil()}}>Retour a l'accueil</Button>
                </CardContent>
             

            </Card>
        )
    }
}
