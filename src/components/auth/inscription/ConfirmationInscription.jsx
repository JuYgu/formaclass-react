
import React, { Component } from 'react'
import AuthService from '../../../services/auth/Auth-service';
import Button  from '@material-ui/core/Button';
import { BeatLoader } from 'react-spinners';
import { Card, Typography } from '@material-ui/core';

export default class ConfirmationInscription extends Component {
    constructor(props){
        super(props);


        this.state = {
            token: this.props.match.params.token,
            requeteOk : false,
            isLoaded: false
        }
    }

    componentDidMount(){
        AuthService.confirmationInscriptionCheckToken(this.state.token)
                    .then(response =>{
                        this.setState({requeteOk: true, isLoaded: true})
                    }, error => {
                        this.setState({isLoaded: true})
                    })
    }

    accueil = () =>{
        this.props.history.push('/inscription');
    }

    render() {

        let rendu;

        if(!this.state.isLoaded){
            rendu = <div className="spinner"> <BeatLoader  loading color=' #0a7568 ' size={72} /> </div>
        }else {
            if(this.state.requeteOk){
                rendu =  
                <>
                    
                    <p>Votre compte vient d'être activé</p>
                    <p>Vous pouvez maintenant vous connecter pour acceder a votre profil </p>
                    
                    <Button onClick={() => {this.accueil()}}>Inscription</Button>
                </>
               
            }else{
                rendu = <>
                
                    
                    <Typography variant="subtitle1">Le lien a expiré</Typography>
                    <Typography variant="subtitle1">Recommencez votre inscription </Typography>
                    
                    <Button color="primary" onClick={() => {this.accueil()}}>Connexion</Button>
                
                </>
            }
        }
        return (
            <Card className="inscription">
                <Typography variant="h1">Inscription</Typography>

            {rendu}

            </Card>
        )
    }
}
