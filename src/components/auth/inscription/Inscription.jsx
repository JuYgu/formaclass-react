import React, { Component, useState } from 'react'

import '../../../css/login.css';

import AuthService from '../../../services/auth/Auth-service.jsx';

import { Box, Button, Card, CardContent, Snackbar, Step, StepLabel, Stepper, Container, Typography } from '@material-ui/core'
import { Form, Formik} from 'formik';
import FormikField from '../../reusableComponent/formikField/Input';
import * as Yup from 'yup';
import FormikFieldOnBlur from './../../reusableComponent/formikField/InputOnBlur';
import Alert from '@material-ui/lab/Alert';
import InscriptionOk from './InscriptionOk';



const initialValues = {

    nom: "",
    prenom: "",
    voie: "",
    codePostal: "",
    ville: "",
    telephone: "",
    authUser: {
        email: "",
        password: "",
        checkpassword: ""
    },
    etablissement: {
        nom: "",
        voie: "",
        ville: "",
        codePostal: "",
        email: "",
        telephone: ""
    }
}




export default class Inscription extends Component {
    constructor(props) {
        super(props);
       
        this.state = {
          message: "",
          errorEtabName : false,
          errorEtabNameMessage : "",
          errorUserName : false,
          errorUserNameMessage : "",
          snackbarOpen: false,
          severity: "",
          inscription: false
        };
      }

    

    inscriptionSchema = Yup.object().shape({
        nom: Yup.string().min(1, "Minimum 1 caractère").max(20, "Maximum 20 caracteres").required("Veuillez indiquer votre nom"),
        prenom: Yup.string().min(3, "Minimum 3 caractères").max(20, "Maximum 20 caracteres").required("Veuillez indiquer votre prenom"),
        voie: Yup.string().min(10, "Minimum 5 caractères").max(50, "Maximum 50 caracteres").required("Champs requis"),
        codePostal: Yup.string().min(5, "Minimum 5 caractère").max(5, "Maximum 5 caracteres").required("Veuillez indiquer votre code postal"),
        ville: Yup.string().min(3, "Minimum 3 caractère").max(20, "Maximum 20 caracteres").required("Veuillez indiquer votre ville"),
        telephone: Yup.string().min(10, "Minimum 1 caractère").max(10, "Maximum 10 caracteres").required("Veuillez indiquer votre téléphone"),
    })

    etablissementSchema = Yup.object().shape({
        etablissement: Yup.object().shape({
            nom: Yup.string().min(4, "Minimum 4 caractère").max(40, "Maximum 40 caracteres").required("Veuillez indiquer le nom de l'établissement"),
            voie: Yup.string().min(10, "Minimum 10 caractère").max(50, "Maximum 50 caracteres").required("Champs requis"),
            ville: Yup.string().min(4, "Minimum 4 caractère").max(40, "Maximum 20 caracteres").required("Veuillez indiquer votre ville"),
            codePostal: Yup.string().min(5, "Minimum 5 caractère").max(5, "Maximum 5 caracteres").required("Veuillez indiquer votre code postal"),
            email: Yup.string().email('Veuillez indiquer une adresse mail valide').required("Veuillez indiquer votre email"),
            telephone: Yup.string().min(10, "Minimum 10 caractère").max(10, "Maximum 10 caracteres").required("Veuillez indiquer le téléphone de l'établissement"),
        })
    })

    authUserSchema = Yup.object().shape({
        authUser: Yup.object().shape({
            email: Yup.string().email().required("Veuillez indiquer votre email"),
            password: Yup.string().min(7, "Minimum 7 caracteres").max(20, "Maximum 20 caracteres").required("Veuillez indiquer votre email"),
        })
    })

    handleSubmit = (values) => {
        AuthService.inscription(values.authUser, values.etablissement, values.nom, values.prenom, values.voie, values.codePostal, values.ville, values.telephone)
        .then(
            (response) => {

                if(response.data.etablissementIsExist || response.data.userIsExist){
                    
                    if(response.data.etablissementIsExist){
                        this.setState({
                            errorEtabName: true,
                            errorEtabNameMessage: "Ce nom d'établissement n'est pas disponible, veuillez le modifier "
                        });
                        var messageEtab = "Ce nom d'établissement n'est pas disponible, veuillez le modifier ";
                    }
                    if(response.data.userIsExist){
                        this.setState({
                            errorUserName: true,
                            errorUserNameMessage: "Cet email est déjà existant. Veuillez en indiquer un autre",
                            severity:"error"
                            
                        });
                        var messageUser = "ce mail n'est pas disponible, veuillez le modifier"
                    }
                    this.setState({snackbarOpen: true, snackbarMsg: `${messageEtab}                                  ${messageUser}`, severity:"error"})


                }else{
                    this.setState({snackbarOpen: true, snackbarMsg: "Votre inscription a bien été validée. Veuillez consulter votre boite mail et cliquer sur le lien pour la valider.", severity:"success", inscription:true})
                    
                }    
            })  
            
        
    }

    onBlurHandle = (e) => {
        var nom = e.target.value;
        AuthService.checkEtablissementName(nom).then((response) =>{
            if(response.data.etablissementIsExist){
                this.setState({
                    errorEtabName: true,
                    errorEtabNameMessage: "Ce nom d'établissement n'est pas disponible"
                })
            }else{
                this.setState({
                    errorEtabName: false,
                    errorEtabNameMessage: ""
                })
            }
        })
    }

    snackBarClose = (event) => {
        this.setState({snackbarOpen: false});
    }

    accueil = () => {
        this.props.history.push("/");
    }


    render() {
        const isAnErrorNameEtab = this.state.errorEtabName;
        const isAnErrorNameUser = this.state.errorUserName;

        const isInscrit = this.state.inscription;
        let rendu;
        if(!isInscrit){
            rendu =
                <>
                    <CardContent className="container">
                        <FormikStepper
                            initialValues={initialValues}
                            onSubmit={ async (values) => 
                                this.handleSubmit
                            }
                        >
                            <FormikStep validationSchema={this.etablissementSchema} label="Informations Etablissement">
                                <Box paddingBottom={1} >
                                <FormikFieldOnBlur label="Nom de l'établissement"  name={`etablissement.nom`}  onblur={ this.onBlurHandle} error={ isAnErrorNameEtab } />
                                <p className="errorMessage">{this.state.errorEtabNameMessage}</p>
                                    {/* <Field label="Nom de l'établissement"  autoComplete="off"  as={TextField} type="text" fullWidth required variant="outlined" name={`etablissement.nom`} onBlur={ this.onBlurHandle}  helperText={<ErrorMessage name={`etablissement.nom`} />}  /> */}
                                </Box>
                                <Box paddingBottom={1} className="box" >
                                    <FormikField label="Email" name={`etablissement.email`} type="email" />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Voie" name={`etablissement.voie`} />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Ville" name={`etablissement.ville`} />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Code Postal" name={`etablissement.codePostal`} />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Telephone" name={`etablissement.telephone`} />
                                </Box>
                            </FormikStep>
                            <FormikStep validationSchema={this.inscriptionSchema} label="Informations Personnelles">
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Nom" name="nom" />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Prenom" name="prenom" />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Voie" name="voie" />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Code Postal" name="codePostal" />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Ville" name="ville" />
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Téléphone" name="telephone" />
                                </Box>
                            </FormikStep>
                            <FormikStep validationSchema={this.authUserSchema} label="Identifiants">
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Email" name={`authUser.email`} type="email"  error={ isAnErrorNameUser } />
                                    <p className="errorMessage">{this.state.errorUserNameMessage}</p>
                                </Box>
                                <Box paddingBottom={1} className="box">
                                    <FormikField label="Password" name={`authUser.password`} type="password" />
                                </Box>
                            </FormikStep>

                        </FormikStepper>
                    </CardContent>
                </>
            
        }else{
            rendu =
                <InscriptionOk history={this.props.history}/>
            
        }
        return (
            <Container>
                <Typography variant="h1" className="titre">FORMACLASS</Typography>
                <Card className="login">
                    <Snackbar
                        anchorOrigin={{vertical:'bottom', horizontal:'center'}}
                        open = {this.state.snackbarOpen}
                        autoHideDuration = {10000}
                        onClose={this.snackBarClose}
                    >
                            <Alert severity={this.state.severity}  onClose={this.snackBarClose}>{this.state.snackbarMsg}</Alert>
                    </Snackbar>

                    {rendu}
                    <Button color="primary" variant="contained" onClick={() => {this.accueil()}}>Accueil</Button>
                </Card>
            </Container>
        );
    }
}

export function FormikStep({ children }) {
    return <>{children}</>
}

export function FormikStepper({ children, errorName, ...props }) {



    const childrenArray = React.Children.toArray(children);
    const [step, setStep] = useState(0);
    const currentChild = childrenArray[step];

    //A voir si on l'utilise... DOit normalement servir a disabled le bouton submit
    // var errorEtabName = {errorName}

    

    function isLastStep() {
        return step === childrenArray.length - 1;
    }
    return (
        <>
        <Typography variant="h3">Inscription</Typography>
        <Formik
         {...props} 
         validationSchema={currentChild.props.validationSchema}
         onSubmit={async (values, helpers) => {
            // Faire ici la vérif du nom de l'établissement
            if (isLastStep()) {
                await props.onSubmit(values, helpers);
            } else {
                setStep(s => s + 1);
            }
        }}>
            {({isSubmitting, dirty, isValid}) => (
                <Form autoComplete="off">
                <Stepper alternativeLabel activeStep={step}>
                    {childrenArray.map((child) => (
                    <Step key={child.props.label}>
                        <StepLabel>{child.props.label}</StepLabel>
                    </Step>
                    ))}
                </Stepper>

                    {currentChild}
                    {step > 0 ? (
                    <Button  disabled={isSubmitting } onClick={() => setStep(s => s - 1)} variant="contained" color="primary">Retour</Button>
                    ) : null}
                    
                    <Button disabled={!dirty || !isValid || isSubmitting } variant="contained" color="primary" type="submit">{isSubmitting ? 'Validation Formulaire' : isLastStep() ? 'Valider' : 'Suivant'}</Button>
              
                   
                </Form>
                )}

        </Formik>
               
        </>
    )



}
