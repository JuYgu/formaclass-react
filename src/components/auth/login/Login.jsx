import React, { Component } from 'react'
import '../../../css/login.css';
import * as Yup from 'yup';
import { Form, Formik } from 'formik';
import FormikField from '../../reusableComponent/formikField/Input.jsx';
import SnackBar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
// import FormikSelect from './../reusableComponent/formikSelect/Select';
import Button from '@material-ui/core/Button';
import AuthService from '../../../services/auth/Auth-service.jsx';
import Recaptcha from 'react-recaptcha';
import { Link, Typography, Card, Container } from '@material-ui/core';




// Voici la liste des catégories. Ici ce n'est qu'un exemple, il faudra aller chercher cette liste dans la bdd
// const categorieItems = [
//     {
//         label: 'Cuisine au beurre',
//         value: 1
//     },
//     {
//         label: 'Informatique',
//         value: 2
//     },
//     {
//         label: 'Gestion',
//         value: 3
//     },
//     {
//         label: 'Genie Civil',
//         value: 4
//     },
// ]

let onLoad 

function Alert(props){
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class Login extends Component {

    constructor(props){
        super(props);

        this.state = {
            snackbarOpen: false,
            snackbarMsg : "",
            severity: "",
            isVerifie: false,
            isLoading : false
        }

    }

    componentDidMount(){
        this.recaptchaLoaded();
        console.log("c'est true")
        

    }

    initialValues = {
        email: "",
        password: "",
      // recaptcha: ""
        // categorie: ""
    }

    LoginSchema = Yup.object().shape({
        email: Yup.string()
            .email('Email invalide')
            .required('Veuillez indiquer votre adresse email'),
        password: Yup.string()
        //mofif selim
            .min(6, "Minimum 6 caratères")
            //.min(7, "Minimum 7 caratères")
            .required('Veuillez indiquer votre password'),
        // categorie: Yup.string()
        //     .required("Ce choix est requis.")
       // recaptcha: Yup.string().required()
    });

    inscription = () => {
        if(!this.state.verifyCallback){
            this.setState({snackbarOpen: true, snackbarMsg:"Veuillez valider le captcha", severity:"error"})
        }else{
            this.props.history.push("/inscription");
        }
        
    }
    forgetPassword = () => {
        if(!this.state.verifyCallback){
            this.setState({snackbarOpen: true, snackbarMsg:"Veuillez valider le captcha", severity:"error"})
        }else{
            this.props.history.push("/forget-password");
        }
        
    }

    snackBarClose = (event) => {
        this.setState({snackbarOpen: false});
    }
    
    recaptchaLoaded = () => {
        console.log('loaded');
        this.setState({
            isLoading: true
        })
        onLoad = true;
    }

    verifyCallback = (response) =>{
        if(response){
            this.setState({
                verifyCallback : true
            })
        }
    }


    handleSubmit = (values) => {

        // if(!this.state.verifyCallback){
        //     this.setState({snackbarOpen: true, snackbarMsg:"Veuillez valider le captcha", severity:"error"})
        // }else{
            AuthService.login(values.email, values.password)
            .then(
                () => {
                    this.setState({snackbarOpen: true, snackbarMsg:"Connexion réussie", severity:"success"});

                }, error => {
                    if(error){
                        this.setState({snackbarOpen: true, snackbarMsg:"Mauvais identifants", severity:"error"});
                    }
                }
            )
        // }

    };

    render() {
 
        return (
            <Container>
                <Typography variant="h1">FORMACLASS</Typography>
                <Card className="login">
                    <Typography variant="h4">Connexion</Typography>
                    <SnackBar
                        anchorOrigin={{vertical:'bottom', horizontal:'center'}}
                        open = {this.state.snackbarOpen}
                        autoHideDuration = {2000}
                        onClose={this.snackBarClose}
                    >
                            <Alert severity={this.state.severity}  onClose={this.snackBarClose}>{this.state.snackbarMsg}</Alert>
                    </SnackBar>
                    {onLoad && 
                        <Formik
                            initialValues={this.initialValues}
                            onSubmit={this.handleSubmit}
                            validationSchema={this.LoginSchema}
                        >
                            {({ dirty, isValid }) => {
                                return (
                                    <Form>
                                        {/*FormikField sont les inputs. Nous devons y passer comme props un label, un name. Le type est optionnel*/}
                                        <FormikField label="Identifiant" name="email" type="email" />
                                        <FormikField label="Password" name="password" type="password" />

                                        {/*FormikSelect est le selectOption. Nous devons y passer comme props un lalel, un name. Le type est optionnel*/}
                                        {/* <FormikSelect name="categorie" label="Catégorie"  items={categorieItems}/> */}
                                        <Link component="button"  variant="body2" onClick={() => { this.forgetPassword() }}>J'ai oublié mon mot de passe</Link>                                    
                                        <div className="button">
                                            <Button disabled={!dirty || !isValid } variant="contained" color="primary" type="submit" >
                                                Valider
                                            </Button>
                                            <Button variant="contained" color="secondary" onClick={this.inscription}>Inscription</Button>
                                        </div>
                                        
                                    </Form>
                                    
                                );
                            }}
                        </Formik>
                    }
                    

                    <div className="captcha">
                        <Recaptcha
                            sitekey="6Le_VUEaAAAAAINnY7gJpODpWtf7oXl6HrzcnVq-"
                            verifyCallback={this.verifyCallback}
                            onloadCallback={this.recaptchaLoaded}
                        />
                    </div>
                </Card>
            </Container>
           
        );
    }
}
