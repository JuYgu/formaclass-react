import axios from 'axios';
import authHeader from '../auth/Auth-header';

const API_URL_EQUIPEMENT = "http://localhost:8080/formaclass/api/equipements";

class EquipementService{

    listeEquipements(){
        return axios
            .get(API_URL_EQUIPEMENT + "liste-equipements", { headers: authHeader() });
    }

    getEquipement(idEquipement){
        return axios
            .get(API_URL_EQUIPEMENT + "equipement/" + idEquipement, { headers: authHeader() });
    }

    addEquipement(nom, quantiteTotale){
        return axios
            .post(API_URL_EQUIPEMENT + "ajouter-equipement", {
                nom: nom,
                quantiteTotale: quantiteTotale
        },
            { headers: authHeader() }
        );
    }

    editEquipement(idEquipement, nom, quantiteTotale){
        return axios
            .put(API_URL_EQUIPEMENT + "equipement/" + idEquipement, {
                nom: nom,
                quantiteTotale: quantiteTotale
        },
            { headers: authHeader() }
        );
    }

    deleteEquipement(idEquipement){
        return axios
            .delete(API_URL_EQUIPEMENT + "equipement/" + idEquipement, { headers: authHeader() });
    }
    
}

export default new EquipementService();