import axios from 'axios';
import authHeader from './../auth/Auth-header';


const API_URL_CATEGORY = "http://localhost:8080/formaclass/api/category/";

class CategoryService{

    getAllCategory(){
        return axios.get(API_URL_CATEGORY+"all", {headers : authHeader()});
    }

    createCategory(type){
        return axios.post(API_URL_CATEGORY+"new", type, {headers : authHeader()});
    }

    getCategoryById(id){
        return axios.get(API_URL_CATEGORY+id, {headers : authHeader()});
    }

    updateCategory(idCategorie , type){
        return axios.put(API_URL_CATEGORY+"update", {
            idCategorie, type
        }, {headers : authHeader()});
    }

    deleteCategory(idCategorie){
        return axios.delete(API_URL_CATEGORY+"delete/"+idCategorie , {headers : authHeader()});
    }
}

export default new CategoryService();