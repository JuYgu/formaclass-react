import axios from 'axios';
import authHeader from './../auth/Auth-header';


const API_URL_MODELE = "http://localhost:8080/formaclass/api/session/";

class SessionService{

    createSession(nomination, dateDebut, dateFin, idModele){
        return axios.post(API_URL_MODELE+"new", {
            nomination, dateDebut, dateFin, idModele
        }, {headers : authHeader()})
    }

    updateSession(idSession, nomination, dateDebut, dateFin, idModele){
        return axios.put(API_URL_MODELE+"update", {
            idSession, nomination, dateDebut, dateFin, idModele
        }, {headers : authHeader()})
    }


    getSession(idSession){
        return axios.get(API_URL_MODELE+idSession, {headers: authHeader()})
    }

    cancelSession(idSession){
        return axios.put(API_URL_MODELE+"cancel/"+idSession, {headers : authHeader()} )
    }

    getCompleteSession(){
        return axios.get(API_URL_MODELE+"completeSession", {headers : authHeader()} )
    }

    getIncompleteSession(){
        return axios.get(API_URL_MODELE+"incompleteSession", {headers : authHeader()} )
    }

    getEndedCancelledSession(){
        return axios.get(API_URL_MODELE+"ended-cancelled-session", {headers : authHeader()} )
    }

    addEleves(idSession, eleves){
        return axios.put(API_URL_MODELE+idSession+"/add-eleves", eleves, {headers : authHeader()} )
    }

    deleteEleves(idSession, idEleve ){
        return axios.get(API_URL_MODELE+idSession+"/delete-eleves/"+idEleve, {headers : authHeader()} )
    }

    addProfesseur(idSession, prof){
        return axios.put(API_URL_MODELE+idSession+"/add-professeur", prof, {headers : authHeader()} )
    }

    deleteProfesseur(idSession){
        return axios.get(API_URL_MODELE+idSession+"/delete-professeur",{headers : authHeader()} )
    }

    addSalle(idSession, salle){
        return axios.put(API_URL_MODELE+idSession+"/add-salle", salle, {headers : authHeader()} )
    }

    deleteSalle(idSession){
        return axios.get(API_URL_MODELE+idSession+"/delete-salle",{headers : authHeader()} )
    }
}

export default new SessionService();