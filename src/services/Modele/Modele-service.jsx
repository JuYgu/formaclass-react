import axios from 'axios';
import authHeader from './../auth/Auth-header';


const API_URL_MODELE = "http://localhost:8080/formaclass/api/modele/";

class ModeleService{

    createModel( titre, eleveMin, eleveMax, description, idCategorie){
        return axios.post(API_URL_MODELE+"new",{
            titre, eleveMin, eleveMax, description, idCategorie
        }, {headers: authHeader()});
    }

    updateModel(idModele, titre, eleveMin, eleveMax, description, idCategorie){
        return axios.put(API_URL_MODELE+"update",{
            idModele, titre, eleveMin, eleveMax, description, idCategorie
        }, {headers: authHeader()});
    }

    getModel( idModele){
        return axios.get(API_URL_MODELE+idModele,  {headers: authHeader()} )
    }

    getAllModels(){
        return axios.get(API_URL_MODELE+"allModeles",  {headers: authHeader()} )
    }



    deleteModel(idModel){
        return axios.delete(API_URL_MODELE+"delete/"+idModel,  {headers: authHeader()})
    }
}

export default new ModeleService();