import axios from 'axios';
import authHeader from '../auth/Auth-header';

const API_URL_SALLE = "http://localhost:8080/formaclass/api/salles/";


class SalleService {

    constructor(props) {

        this.state = {

            listeSalle: [],
            idSalle: ""
        }
    }

    listeSalles() {
        return axios
            .get(API_URL_SALLE + "liste-salles", { headers: authHeader() });
    }

    getSalle(idSalle) {
        return axios
            .get(API_URL_SALLE + "salle/" + idSalle, { headers: authHeader() });
    }

    editSalle(idSalle, nom, capaciteMax, accessibilite, reservable) {
        return axios
            .put(API_URL_SALLE + "salle/" + idSalle, {
                nom: nom,
                capaciteMax: capaciteMax,
                accessibilite: accessibilite,
                reservable: reservable
            },
                { headers: authHeader() }
            );
    }

    addSalle(nom, capaciteMax, accessibilite, reservable) {
        return axios
            .post(API_URL_SALLE + "ajouter-salle", {
                nom: nom,
                capaciteMax: capaciteMax,
                accessibilite: accessibilite,
                reservable: reservable
            },
                { headers: authHeader() }
            );

    }

    deleteSalle(idSalle) {
        return axios
            .delete(API_URL_SALLE + "salle/" + idSalle, { headers: authHeader() })
    }


    findSallesByCriteria(debutSession, finSession, accessibilite, capaciteMax){
        return axios.post(API_URL_SALLE+"find", {
            debutSession, finSession, accessibilite, capaciteMax
        },  { headers: authHeader() } )
    }

}

export default new SalleService();