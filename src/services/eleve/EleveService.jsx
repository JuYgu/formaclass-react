import axios from 'axios';
import authHeader from '../auth/Auth-header';

const API_URL_ELEVE = "http://localhost:8080/formaclass/api/eleves/";
const API_URL_SESSION = "http://localhost:8080/formaclass/api/session/";
const API_URL_CSV = "http://localhost:8080/formaclass/api/csv/";



class EleveService {

 
    listeEleves() {
        return axios
            .get(API_URL_ELEVE + "liste-eleves", { headers: authHeader() });
    }

    getEleve(idEleve) {
        return axios
            .get(API_URL_ELEVE + "eleve/" + idEleve, { headers: authHeader() });
    }

    editEleve(idEleve, nom, prenom, voie, ville, codePostal, telephone, authUser) {
         return axios
            .put(API_URL_ELEVE + "eleve/" + idEleve, {
                nom :  nom, prenom : prenom, voie : voie, ville : ville, codePostal : codePostal, telephone : telephone, authUser : authUser
               
            },
                 { headers: authHeader() }
            );
    }

    addEleve(nom, prenom, voie, ville, codePostal, telephone, email) {
        return axios
            .post(API_URL_ELEVE + "ajouter-eleve", {
               nom :  nom, prenom : prenom, voie : voie, ville : ville, codePostal : codePostal, telephone : telephone, email : email
            },
                { headers: authHeader() }
            );

    }

    deleteEleve(idEleve) {
        return axios
            .delete(API_URL_ELEVE + "eleve/" + idEleve, { headers: authHeader() })
    }

    getSessionEleve(idEleve) {
        return axios.get(API_URL_SESSION + "eleve-session/" + idEleve, { headers: authHeader() })
    }

    getListSessionEleve(idEleve) {
        return axios.get(API_URL_SESSION + "eleve-listesession/" + idEleve, { headers: authHeader() })
    }

    postCsv(data){
        return axios.post(API_URL_CSV + "upload", data, {  headers: authHeader()})    
    }
}

export default new EleveService();