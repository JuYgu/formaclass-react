import axios from 'axios';
import authHeader from '../auth/Auth-header';

const API_URL_ELEVE = "http://localhost:8080/formaclass/api/eleves/";


class EleveService {







    findElevesByCriteria(idModele, dateDebut, dateFin){
        return axios.post(API_URL_ELEVE+"find", {
            idModele, dateDebut, dateFin
        }, { headers: authHeader() })
    }

}
export default new EleveService()
