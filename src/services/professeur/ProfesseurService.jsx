import axios from 'axios';
import authHeader from '../auth/Auth-header';

const API_URL_PROF = "http://localhost:8080/formaclass/api/professeurs/";
const API_URL_SESSION = "http://localhost:8080/formaclass/api/session/";


class ProfesseurService {

 
    listeProfesseurs() {
        return axios
            .get(API_URL_PROF + "liste-professeurs", { headers: authHeader() });
    }

    getProfesseur(idProfesseur) {
        return axios
            .get(API_URL_PROF + "professeur/" + idProfesseur, { headers: authHeader() });
    }

    editProfesseur(idProfesseur, nom, prenom, voie, ville, codePostal, telephone, authUser) {
         return axios
            .put(API_URL_PROF + "professeur/" + idProfesseur, {
                nom :  nom, prenom : prenom, voie : voie, ville : ville, codePostal : codePostal, telephone : telephone, authUser : authUser
               
            },
                 { headers: authHeader() }
            );
    }

    addProfesseur(nom, prenom, voie, ville, codePostal, telephone, authUser) {
        return axios
            .post(API_URL_PROF + "ajouter-professeur", {
               nom :  nom, prenom : prenom, voie : voie, ville : ville, codePostal : codePostal, telephone : telephone, authUser : authUser
            },
                { headers: authHeader() }
            );

    }

    deleteProfesseur(idProfesseur) {
        return axios
            .delete(API_URL_PROF + "professeur/" + idProfesseur, { headers: authHeader() })
    }




    findProfsByCriteria(idCategorie, dateDebut, dateFin){
        return axios.post(API_URL_PROF+"find", {
            idCategorie, dateDebut, dateFin
        },  { headers: authHeader() } )
    }


    getSessionProfesseur(idProfesseur) {
        return axios.get(API_URL_SESSION + "professeur-session/" + idProfesseur, { headers: authHeader() })
    }

    getListSessionProfesseur(idProfesseur) {
        return axios.get(API_URL_SESSION + "professeur-listsessions/" + idProfesseur, { headers: authHeader() })
    }
}

export default new ProfesseurService();