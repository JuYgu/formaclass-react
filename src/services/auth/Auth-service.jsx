import axios from 'axios';
import AuthHeaderPass from './Auth-header-password';


const API_URL_AUTH = "http://localhost:8080/formaclass/api/auth/";

class AuthService{
    
    login(email, password){
        return axios
                    .post(API_URL_AUTH+"login", {
                        email, password
                    })
                    .then(response => {
                        console.log("dans AuthService - login");
                        console.log("response", response.data);
                        if ( response.data.token ){
                            localStorage.setItem("user", JSON.stringify(response.data.token));
                            localStorage.setItem("xmnr", JSON.stringify(response.data.idEtablissement));
                            localStorage.setItem("role", JSON.stringify(response.data.roles));
                            localStorage.setItem("idUser", JSON.stringify(response.data.idUser));
                        }
                    });
    }

    logout(){
        localStorage.removeItem("user");
        localStorage.removeItem("xmnr");
        localStorage.removeItem("role");
        localStorage.removeItem("idUser");
    }

    inscription(authUser, etablissement, nom, prenom, voie, codePostal, ville, telephone){
        return axios
                    .post(API_URL_AUTH+"inscription" , {
                        authUser, etablissement, nom, prenom, voie, codePostal, ville, telephone
                    })             
    }

    confirmationInscriptionCheckToken(token){
        return axios.get(API_URL_AUTH+"confirmation-inscription/"+token);
    }

    checkEtablissementName(nom){
        return axios.post(API_URL_AUTH+"check/etablissement",{
            nom
        });
    }

    forgetPasswordPostMail(email){
        return axios.post(API_URL_AUTH+"forget-password",{
            email
        });
    }

    forgetPasswordCheckToken(token){
        return axios.get(API_URL_AUTH+"forget-password/"+token);
    }
 
    forgetPasswordNewPass(token, password){
        return axios.post(API_URL_AUTH+"forget-password/new-password",password,  {headers: AuthHeaderPass(token) });
    }
}

export default new AuthService();