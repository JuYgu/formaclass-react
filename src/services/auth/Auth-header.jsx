
export default function authHeader(){

    const currentUser = JSON.parse(localStorage.getItem("user"));
    const encryptId = JSON.parse(localStorage.getItem("xmnr"));


    if(currentUser){
        return {Authorization: "Bearer " + currentUser, xmnr: encryptId};
    }else{
        return {};
    }
}

