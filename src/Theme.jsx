import { createMuiTheme } from "@material-ui/core";

const Theme = createMuiTheme({
    palette: {
        primary: {
            main: "#104654",
            light:"#408697"
        },
        secondary:{
            main: "#C9A617",
            light: "#f0d774"
        },
        neutral:{
            main: "white"
        }
    },
    shape:{
        borderRadius: 30
    },
    typography: {
        body2:{
                
                '@media (max-width:400px)': {
                    fontSize: '10px',
                },
         
        },

    },
    overrides:{
        body2:{
            root:{
                '@media (max-width:400px)': {
                    width: '20%',
                },
            }
        },
        MuiButton:{

            contained:{
                "&$disabled":{
                    backgroundColor: "rgba(0, 0, 0, 0.24)"
                },
                '&:hover' : {
                    boxShadow: "0px 12px 4px -1px rgba(9, 8, 8, 0.57),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)" 
                }
            },
            containedPrimary:{
                '&:hover':{
                    backgroundColor: "rgb(74, 164, 134)"
                }
            }
           

        },
        MuiIconButton:{
                root:{
                    '&:hover' : {
                        boxShadow: "0px 12px 4px -1px rgba(9, 8, 8, 0.57),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)" 
                    }
                }
               
            
        },
        MuiTextField: {
            fullWidth: {
                maxWidth: '100%'
            },
            root:{
                '@media (max-width:400px)': {
                    width: '100%',
                },
            }
        },
        MuiBox : {
            width: "100%",
            root : {
                width: "100%"
            }
        },
        MuiInputBase:{
            variant: "filled",
            root:{
                color:"white",
                '&:hover' : {
                    boxShadow: " 10px 10px 20px -4px rgba(0,0,0,0.75)" ,
                    transition : "box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
                }
            },
            input : {

                      textAlign: "center"
                   
            }
            
        },  
        MuiFormLabel:{
            root:{
                "&$focused": {
                    color: "#C9A617"
                },
            },
          

        },
        MuiFormHelperText:{
            root:{
                "&$required":{
                    color: "#712219"
                }
            }
        },
        MuiInput:{
            
            underline:{
                '&:after':{
                    borderBottomColor : "#C9A617"
                    }
                }

        },  
        MuiInputLabel:{
           
            root:{
                color: "white",
            },
            asterisk:{
                color:"#712219"
            }
        },
        // MuiContainer: {
        //     maxWidth: 'md',
        //     root: {
        //         '@media (max-width:400px)': {
        //             width: '80%',
        //         },

        //     }
        // },

        MuiCard: {
           // maxWidth: 'md',
            elevation:12,
            width: "100%",
            padding: "30px",
            root: {
                backgroundColor: "#ffffff2b",
                '&:hover' : {
                    boxShadow: "10px 10px 20px -4px rgba(0,0,0,0.75)" ,
                    transition : "box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
                }

            },

        },
        MuiTableContainer: {
            root: {
                '&:hover' : {
                    boxShadow: "10px 10px 20px -4px rgba(0,0,0,0.75)" ,
                    transition : "box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
                }

            },
        },
        // MuiTableRow: {
        //     root: {
        //         '&:hover' : {
        //             boxShadow: "10px 10px 20px -4px rgba(0,0,0,0.75)" ,
        //             transition : "box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
        //         }

        //     },
        // },
        MuiTableCell : {
            head : {
                backgroundColor: "#104654",
                color: "white"
            }
        },
        MuiStepper:{
            root:{
                backgroundColor: "#fff0",
                '@media (max-width:400px)': {
                    display: "none"
                 },
               
            }
        },
        MuiStepLabel:{
            label:{
                color: "white",
                "&$active":{
                    color:"white"
                }
            },
            root:{
                '@media (max-width:400px)': {
                   display: "none"
                },
            }
        },
       MuiStepIcon:{
           root:{
                color: "#C9A617",
                "&$active":{
                    color: "#104654"
                }

           }
             
       },
    
        MuiTypography:{
            h1: {
                fontSize: "3.5em",
                '@media (max-width:400px)': {
                    fontSize: '2.5rem',
                },
                fontWeight: "bold",
                color: "white",
                paddingBottom: "30px",
                textAlign: "center"
            },
            h3:{
                color: "white",
               
                marginBottom: "20px",
                textAlign: "center",
                '@media (max-width:400px)': {
                    fontSize: '2.5rem',
                },
            },
            h4:{
                color: "white",
                paddingBottom: "30px",
                textAlign: "center"
            },
            subtitle1:{
                color:"white"
            }
            
        },

    }
    
})

export default Theme;