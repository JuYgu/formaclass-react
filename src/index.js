import React from 'react';
import ReactDOM from 'react-dom';

import 'typeface-roboto';
import './index.css';
import App from './components/App/App.jsx';




ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


